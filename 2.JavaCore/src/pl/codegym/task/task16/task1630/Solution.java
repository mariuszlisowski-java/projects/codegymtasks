package pl.codegym.task.task16.task1630;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Solution {
    public static String firstFileName;
    public static String secondFileName;

    static {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            firstFileName = reader.readLine();
            secondFileName = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        systemOutPrintln(firstFileName);
        systemOutPrintln(secondFileName);
    }

    public static void systemOutPrintln(String fileName) throws InterruptedException {
        ReadFileInterface f = new ReadFileThread();
        f.setFileName(fileName);
        f.start();

        f.join();

        System.out.println(f.getFileContents());
    }

    public interface ReadFileInterface {
        void setFileName(String fullFileName);
        String getFileContents();
        void join() throws InterruptedException;
        void start();
    }

    public static class ReadFileThread extends Thread implements ReadFileInterface {
        private String filename;
        private List<String> lines;

        @Override
        public void setFileName(String fullFileName) {
            this.filename = fullFileName;
        }

        @Override
        public String getFileContents() {
            if (lines == null) {
                return "";
            }
            StringBuilder output = new StringBuilder();
            for (String line : lines) {
                output.append(line).append(" ");
            }

            return output.toString();
        }

        @Override
        public void run() {
            try {
                lines = Files.readAllLines(Paths.get(filename));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
