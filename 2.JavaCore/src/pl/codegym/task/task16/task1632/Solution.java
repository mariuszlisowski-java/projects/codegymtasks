package pl.codegym.task.task16.task1632;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static {
        threads.add(new Thread1());
        threads.add(new Thread2());
        threads.add(new Thread3());
        threads.add(new Thread4());
        threads.add(new Thread5());
    }

    public static void main(String[] args) {
        threads.get(1).start();
        threads.get(1).interrupt();
    }

    private static class Thread1 extends Thread {
        public void run() {
            while (true) {}
        }
    }

    private static class Thread2 extends Thread {
        public void run() {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
                interrupt();
            }
        }
    }

    private static class Thread3 extends Thread {
        public void run() {
            while (!isInterrupted()) {
                System.out.println("Hurra");
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public static class Thread4 extends Thread implements Message {
        public void run() {
            while (!isInterrupted()) {}
        }

        @Override
        public void showWarning() {
            this.interrupt();
        }
    }

    private static class Thread5 extends Thread {
        public void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int sum = 0;
            String input = "";
            try {
                do {
                    input = reader.readLine();
                    try {
                        sum += Integer.parseInt(input);
                    } catch (NumberFormatException e) {
                    }
                } while (!input.equals("N"));
                System.out.println(sum);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}