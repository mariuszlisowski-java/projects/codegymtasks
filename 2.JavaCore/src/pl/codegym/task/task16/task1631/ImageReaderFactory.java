package pl.codegym.task.task16.task1631;

import pl.codegym.task.task16.task1631.common.*;

public class ImageReaderFactory {
    public static ImageReader getImageReader(ImageTypes type) {
        if (type != null) {
            switch (type) {
                case BMP:
                    return new BmpReader();
                case JPG:
                    return new JpgReader();
                case PNG:
                    return new PngReader();
                default:
                    throw new IllegalArgumentException("Nieznany typ obrazu");
            }
        } else {
            throw new IllegalArgumentException();
        }
    }
}
