package pl.codegym.task.task16.task1617;

/* 
Odliczanie na wyścigach
*/

import java.util.Date;

public class Solution {
    public static volatile int numSeconds = 3;

    public static void main(String[] args) throws InterruptedException {
        RacingClock clock = new RacingClock();

        Thread.sleep(3500);
        clock.interrupt();
    }

    public static class RacingClock extends Thread {
        public RacingClock() {
            start();
        }

        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    if (numSeconds == 0) {
                        System.out.println("Start!");
                        break;
                    } else {
                        System.out.print(numSeconds + " ");
                    }
                    Thread.sleep(1000);
                    --numSeconds;
                } catch (InterruptedException e) {
                    System.out.println("Przerwane!");
                    Thread.currentThread().interrupt();
                }
            }




        }
    }

}
