package pl.codegym.task.task17.task1711;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<>();

    static {
        allPeople.add(Person.createMale("Donald Chump", new Date()));  // id=0
        allPeople.add(Person.createMale("Larry Gates", new Date()));  // id=1
    }

    public static void main(String[] args) throws ParseException {
        if (args.length > 0) {
            switch (args[0]) {
                case "-c":
                    synchronized (allPeople) {
                        for (int i = 1; i < args.length; i += 3) {
                            addPerson(args[i], args[i + 1], args[i + 2]);
                        }
                        break;
                    }
                case "-u":
                    synchronized (allPeople) {
                        for (int i = 1; i < args.length; i += 4) {
                            updatePerson(args[i], args[i + 1], args[i + 2], args[i + 3]);
                        }
                        break;
                    }
                case "-d":
                    synchronized (allPeople) {
                        for (int i = 1; i < args.length; i++) {
                            deletePerson(args[i]);
                        }
                        break;
                    }
                case "-i":
                    synchronized (allPeople) {
                        for (int i = 1; i < args.length; i++) {
                            getPersonInfo(args[i]);
                        }
                        break;
                    }
            }
        }
    }

    private static void addPerson(String name, String sex, String bd) throws ParseException {
        DateFormat dateFormat =  new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH);
        if (sex.equals("m")) {
            allPeople.add(Person.createMale(name, dateFormat.parse(bd)));
        } else {
            allPeople.add(Person.createFemale(name, dateFormat.parse(bd)));
        }
        System.out.println(allPeople.size() - 1);
    }

    private static void updatePerson(String id, String name, String sex, String bd) throws ParseException {
        DateFormat dateFormat =  new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH);
        if (sex.equals("m")) {
            allPeople.set(Integer.parseInt(id), Person.createMale(name, dateFormat.parse(bd)));
        } else {
            allPeople.set(Integer.parseInt(id), Person.createFemale(name, dateFormat.parse(bd)));
        }
    }

    private static void deletePerson(String id) {
        Person person = Person.createMale(null, null);
        person.setSex(null);
        allPeople.set(Integer.parseInt(id), person);
    }

    private static void getPersonInfo(String id) {
        DateFormat dateFormat =  new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
        Person person = allPeople.get(Integer.parseInt(id));

        System.out.println(person.getName() + " " + (person.getSex() == Sex.MALE ? 'm' : 'f') + " " +
                dateFormat.format(person.getBirthDate()));
    }


}