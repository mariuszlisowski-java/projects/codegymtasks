package pl.codegym.task.task17.task1721;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Transakcyjność
*/

public class Solution {
    public static List<String> allLines = new ArrayList<>();
    public static List<String> linesForRemoval = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(filename1));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                allLines.add(line);
            }
            printList(allLines);
            bufferedReader = new BufferedReader(new FileReader(filename2));
            while ((line = bufferedReader.readLine()) != null) {
                linesForRemoval.add(line);
            }
            printList(linesForRemoval);
        } catch (FileNotFoundException e) {
            // no-op
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }

        Solution solution = new Solution();
        solution.joinData();
    }

    public void joinData() throws IOException {
        if (allLines.containsAll(linesForRemoval)) {
            allLines.removeAll(linesForRemoval);
            printList(allLines);
        } else {
            allLines.clear();
            throw new CorruptedDataException();
        }

    }

    private static <T> void printList(List<T> list) {
        for (T element : list) {
            System.out.println(element);
        }
        System.out.println();
    }

}
