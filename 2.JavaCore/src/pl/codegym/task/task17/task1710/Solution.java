package pl.codegym.task.task17.task1710;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<>();

    static {
        allPeople.add(Person.createMale("Donald Chump", new Date()));  // id=0
        allPeople.add(Person.createMale("Larry Gates", new Date()));  // id=1
    }

    public static void main(String[] args) throws ParseException {
        if (args.length > 0) {
            if (args[0].equals("-c") && args.length == 4) {
                addPerson(args[1], args[2], args[3]);
            } else if (args[0].equals("-u") && args.length == 5) {
                updatePerson(args[1], args[2], args[3], args[4]);
            } else if (args[0].equals(("-d")) && args.length == 2) {
                deletePerson(args[1]);
            } else if (args[0].equals("-i") && args.length == 2) {
                getPersonInfo(args[1]);
            }
        }
    }

    private static void addPerson(String name, String sex, String bd) throws ParseException {
        DateFormat dateFormat =  new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH);
        if (sex.equals("m")) {
            allPeople.add(Person.createMale(name, dateFormat.parse(bd)));
        } else {
            allPeople.add(Person.createFemale(name, dateFormat.parse(bd)));
        }
        System.out.println(allPeople.size() - 1);
    }

    private static void updatePerson(String id, String name, String sex, String bd) throws ParseException {
        DateFormat dateFormat =  new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH);
        if (sex.equals("m")) {
            allPeople.set(Integer.parseInt(id), Person.createMale(name, dateFormat.parse(bd)));
        } else {
            allPeople.set(Integer.parseInt(id), Person.createFemale(name, dateFormat.parse(bd)));
        }
    }

    private static void deletePerson(String id) {
        Person person = Person.createMale(null, null);
        person.setSex(null);
        allPeople.set(Integer.parseInt(id), person);
    }

    private static void getPersonInfo(String id) {
        DateFormat dateFormat =  new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
        Person person = allPeople.get(Integer.parseInt(id));
        Date birthDate = person.getBirthDate();

        System.out.println(person.getName() + " " + (person.getSex() == Sex.MALE ? 'm' : 'f') + " " +
                           dateFormat.format(person.getBirthDate()));
    }



}
