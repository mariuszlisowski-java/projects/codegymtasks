package pl.codegym.task.task14.task1420;

/* 
NWD
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int a = 0, b = 0;
        int result = 0;
        try {
            a = Integer.parseInt(reader.readLine());
            b = Integer.parseInt(reader.readLine());
        } catch (Exception e) {
            e.getStackTrace();
        }

        if (a <= 0 || b <= 0){
            throw new Exception();
        }

        System.out.println(nwd(a, b));
        reader.close();
    }

    public static int nwd(int a, int b){
        int min = a;
        if (b < a) {
            min = b;
        }
        for (int i = min; i >= 1; i--) {
            if (a % i == 0 && b % i == 0) {
                return i;
            }
        }
        return 1;
    }

}
