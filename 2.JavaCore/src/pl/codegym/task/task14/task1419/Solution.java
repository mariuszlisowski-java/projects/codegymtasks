package pl.codegym.task.task14.task1419;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/* 
Inwazja wyjątków
*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   // Pierwszy wyjątek
        try {
            float i = 1 / 0;
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            exceptions.get(-1);
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            String str = null;
            System.out.println(str.charAt(0));
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            File file = new File("Non existing file");
            FileReader fileReader = new FileReader(file);
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            int number = Integer.parseInt("not a number");
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            throw new InterruptedException();
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            Class loadedClass = Class.forName("main.java.Utils");
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            throw new IllegalArgumentException();
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            throw new ClassCastException();
        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            throw new StringIndexOutOfBoundsException();
        } catch (Exception e) {
            exceptions.add(e);
        }
    }
}
