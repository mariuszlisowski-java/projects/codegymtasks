package pl.codegym.task.task13.task1326;

/* 
Sortowanie liczb parzystych z pliku
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String filename = "test.ints";
        String filename = reader.readLine();

        InputStream input = new FileInputStream(filename);
        InputStreamReader stream = new InputStreamReader(input);
        BufferedReader buffer = new BufferedReader(stream);

        String line;
        List<Integer> numbers = new ArrayList<>();
        while ((line = buffer.readLine()) != null) {
            try {
                int number = Integer.parseInt(line);
                if (number % 2 == 0) {
                    numbers.add(number);
                }
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }

        buffer.close();
        stream.close();
        input.close();

        numbers.stream()
                .sorted(Comparator.naturalOrder())
                .forEach(System.out::println);
    }

}
