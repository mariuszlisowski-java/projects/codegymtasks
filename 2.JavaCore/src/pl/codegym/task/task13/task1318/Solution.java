package pl.codegym.task.task13.task1318;

import java.io.*;

/* 
Wczytywanie pliku
*/

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        InputStream input = null;
        BufferedReader buffer = null;

        try {
//            String filename = "test.text";
            String filename = reader.readLine();
            input = new FileInputStream(filename);
            buffer = new BufferedReader(new InputStreamReader(input));
            String line;
            while ((line = buffer.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
                if (input != null) {
                    input.close();
                }
                if (buffer != null) {
                    buffer.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

    }

}