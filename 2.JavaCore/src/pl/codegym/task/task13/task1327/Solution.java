package pl.codegym.task.task13.task1327;

import java.util.ArrayList;
import java.util.List;

/* 
Rzepka
*/

public class Solution {
    public static void main(String[] args) {
        List<Person> plot = new ArrayList<>();
        plot.add(new Person("Turnip"));
        plot.add(new Person("Grandpa"));
        plot.add(new Person("Grandma"));
        plot.add(new Person("Granddaughter"));
        TurnipStory.tell(plot);
    }

    public static class Person implements TurnipItem {
        String name;

        public Person(String name) {
            this.name = name;
        }
        @Override
        public String getName() {
            return this.name;
        }

        public void pull(Person person) {
            System.out.println(name + " behind " + person.getName());
        }
    }

    public static class TurnipStory {
        static void tell(List<Person> items) {
            Person first;
            Person second;
            for (int i = items.size() - 1; i > 0; i--) {
                first = items.get(i - 1);
                second = items.get(i);
                second.pull(first);
            }
        }
    }

}
