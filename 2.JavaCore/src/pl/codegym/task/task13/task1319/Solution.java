package pl.codegym.task.task13.task1319;

import java.io.*;

/* 
Zapisywanie do pliku z konsoli
*/

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        OutputStream output = new FileOutputStream(filename);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));

        String line;
        do {
            line = reader.readLine();
            writer.write(line + '\n');
        } while (!(line.equals("exit")));

        reader.close();
        writer.close();
        output.close();
    }

}
