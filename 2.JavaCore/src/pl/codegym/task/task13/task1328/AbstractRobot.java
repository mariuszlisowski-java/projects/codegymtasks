package pl.codegym.task.task13.task1328;

public abstract class AbstractRobot implements CanAttack, CanDefend {
    private static int hitCount;
    private String name;

    public AbstractRobot(String name) {
        this.name = name;
        hitCount = 0;
    }

    public String getName() {
        return name;
    }

    public BodyPart attack() {
        BodyPart attackedBodyPart = null;
        hitCount = hitCount + 1;

        if (hitCount == 1) {
            attackedBodyPart = BodyPart.ARM;
        } else if (hitCount == 2) {
            attackedBodyPart = BodyPart.HEAD;
        } else if (hitCount == 3) {
            attackedBodyPart = BodyPart.LEG;
        } else if (hitCount == 4) {
            attackedBodyPart = BodyPart.CHEST;
        }
        return attackedBodyPart;
    }

    public BodyPart defend() {
        BodyPart defendedBodyPart = null;

        if (hitCount == 1) {
            defendedBodyPart = BodyPart.ARM;
        } else if (hitCount == 2) {
            defendedBodyPart = BodyPart.HEAD;
        } else if (hitCount == 3) {
            defendedBodyPart = BodyPart.LEG;
        } else if (hitCount == 4) {
            hitCount = 0;
            defendedBodyPart = BodyPart.CHEST;
        }
        return defendedBodyPart;
    }

}
