package pl.codegym.task.task15.task1519;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/* 
Różne metody dla różnych typów
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input;
        while (!(input = reader.readLine()).equals("exit")) {
            try {
                if (input.contains(".")) {
                    Double value = Double.parseDouble(input);
                    print(value);
                } else {
                    int value = Integer.parseInt(input);
                    if (value > 0 && value < 128) {
                        print((short) value);
                    } else {
                        print(value);
                    }
                }
            } catch (NumberFormatException | NullPointerException e) {
                print(input);
            }
        }
    }

    public static void print(Double value) {
        System.out.println("To jest Double. Value: " + value);
    }

    public static void print(String value) {
        System.out.println("To jest String. Value: " + value);
    }

    public static void print(short value) {
        System.out.println("To jest liczba całkowita typu short. Value: " + value);
    }

    public static void print(Integer value) {
        System.out.println("To jest liczba całkowita typu Integer. Value: " + value);
    }
}
