package pl.codegym.task.task15.task1522;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Praca nad wzorcem projektowym singleton
*/

public class Solution {
    public static void main(String[] args) {
//        new Solution();
    }

    public static Planet thePlanet;

    static {
        readKeyFromConsoleAndInitPlanet();
    }

    public static void readKeyFromConsoleAndInitPlanet() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals(Planet.SUN)) {
            thePlanet = Sun.getInstance();
        } else if (input.equals(Planet.MOON)) {
            thePlanet = Moon.getInstance();
        } else if (input.equals(Planet.EARTH)){
            thePlanet = Earth.getInstance();
        } else {
            thePlanet = null;
        }

    }
}
