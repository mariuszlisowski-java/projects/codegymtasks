package pl.codegym.task.task15.task1529;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Praktyka z blokami statycznymi
*/

public class Solution {
    public static void main(String[] args) {

    }

    static {
        reset();
    }

    public static CanFly result;

    public static void reset() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        try {
            input = reader.readLine();

            if (input.equals("helikopter")) {
                result = new Helicopter();
            } else if (input.equals("samolot")) {
                int passengers = reader.read();
                result = new Plane(passengers);
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
