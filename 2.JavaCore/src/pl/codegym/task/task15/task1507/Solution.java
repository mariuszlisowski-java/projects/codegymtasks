package pl.codegym.task.task15.task1507;

/* 
OOP: przeciążanie metod
*/

public class Solution {
    public static void main(String[] args) {
        printMatrix(2, 3, "8");
    }

    public static void printMatrix(int m, int n, String value) {
        System.out.println("Wypełnianie obiektami typu String");
        printMatrix(m, n, (Object) value);
    }

    public static void printMatrix(int m, int n, Object value) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(value);
            }
            System.out.println();
        }
    }

    public static void printMatrix(Integer m, Integer n, String value) {
    }
    public static void printMatrix(Double m, Double n, String value) {
    }
    public static void printMatrix(Short m, Short n, String value) {
    }
    public static void printMatrix(Byte m, Byte n, String value) {
    }
    public static void printMatrix(int m, double n, String value) {
    }
    public static void printMatrix(double m, int n, String value) {
    }
    public static void printMatrix(short m, byte n, String value) {
    }
    public static void printMatrix(byte m, short n, String value) {
    }

}
