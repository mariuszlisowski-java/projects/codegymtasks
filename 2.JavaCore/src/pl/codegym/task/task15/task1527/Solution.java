package pl.codegym.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Parser żądań
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String url = reader.readLine();

        List<Double> parameterDouble = new ArrayList<>();
        List<String> parameterString = new ArrayList<>();

        String params = url.substring(url.indexOf('?') + 1);
        int andMark = 0;

        do {
            andMark = params.indexOf('&');
            String param;
            if (andMark != -1) {
                param = params.substring(0, andMark);
                params = params.substring(andMark + 1);
            } else {
                param = params;
            }

            String left = null;
            String right = null;
            if (param.contains("=")) {
                left = param.substring(0, param.indexOf('='));
                right = param.substring(param.indexOf('=') + 1);
            } else {
                left = param;
            }
            System.out.print(left + " ");

            if ("obj".equals(left)) {
                try {
                    parameterDouble.add(Double.parseDouble(right));
                } catch (NullPointerException | NumberFormatException e) {
                    parameterString.add(right);
                }
            }


        } while (andMark != -1);

        System.out.println();
        if (!parameterDouble.isEmpty()) {
            for (Double parameter : parameterDouble) {
                alert(parameter);
            }
        }
        if (!parameterString.isEmpty()) {
            for (String parameter : parameterString) {
                alert(parameter);
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
