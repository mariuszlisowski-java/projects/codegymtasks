package pl.codegym.task.task15.task1523;

/* 
Przeciążanie konstruktorów
*/

public class Solution {
    Solution() {}
    public Solution(int value) {}
    private Solution(double value) {}
    protected Solution(short value) {}

    public static void main(String[] args) {

    }
}

