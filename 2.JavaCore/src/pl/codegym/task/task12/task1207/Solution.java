package pl.codegym.task.task12.task1207;

/* 
int i Integer
*/

public class Solution {
    public static void main(String[] args) {
        print(2);
        print(Integer.valueOf(2));
    }

    public static void print(int value) {}
    public static void print(Integer value) {}
}
