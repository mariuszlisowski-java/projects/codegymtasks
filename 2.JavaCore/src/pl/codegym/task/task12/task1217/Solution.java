package pl.codegym.task.task12.task1217;

/* 
Lataj, biegaj i pływaj
*/

public class Solution {
    public static void main(String[] args) {

    }

    public interface CanFly {
        void fly();
    }
    public interface CanRun {
        void run();
    }
    public interface CanSwim {
        void swim();
    }

}
