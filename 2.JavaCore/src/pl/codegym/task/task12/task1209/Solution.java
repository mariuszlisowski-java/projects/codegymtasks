package pl.codegym.task.task12.task1209;

/* 
Trzy metody i minimum
*/

public class Solution {
    public static void main(String[] args) {

    }

    static int min(int i1, int i2) {
        return Math.min(i1, i2);
    }
    static long min(long l1, long l2) {
        return Math.min(l1, l2);
    }
    static double min(double d1, double d2) {
        return Math.min(d1, d2);
    }
}
