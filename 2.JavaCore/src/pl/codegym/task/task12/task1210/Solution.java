package pl.codegym.task.task12.task1210;

/* 
Trzy metody i maksimum
*/

public class Solution {
    public static void main(String[] args) {

    }

    static int max(int valA, int valB) {
        return Math.max(valA, valB);
    }
    static long max(long valA, long valB) {
        return Math.max(valA, valB);
    }
    static double max(double valA, double valB) {
        return Math.max(valA, valB);
    }
}
