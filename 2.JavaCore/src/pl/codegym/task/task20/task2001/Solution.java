package pl.codegym.task.task20.task2001;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* 
Odczytywanie i zapisywanie do pliku: Human
*/

public class Solution {

    public static void main(String[] args) {
        // Zaktualizuj ciąg przekazany do metody createTempFile na podstawie ścieżki do pliku na dysku twardym
        try {
            File yourFile = File.createTempFile("your_file_name", null);
//            File yourFile = new File("your_file_name");
            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            Human smith = new Human("Smith", new Asset("dom", 999_999.99),
                                                    new Asset("samochód", 2999.99));
//            Human smith = new Human("Smith", null);
            smith.save(outputStream);
            outputStream.flush();
            outputStream.close();

            Human somePerson = new Human();
            somePerson.load(inputStream);
            inputStream.close();

            if (smith.equals(somePerson)) {
                System.out.println("Same persons");
            } else {
                System.out.println("Different persons");
            }

        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("Oops, coś jest nie tak z moim plikiem");
        } catch (Exception e) {
            // e.printStackTrace();
            System.out.println("Oops, coś jest nie tak z metodą save/load");
        }
    }

    public static class Human {

        public String name;
        public List<Asset> assets = new ArrayList<>();

        public Human() {
        }

        public Human(String name, Asset... assets) {
            this.name = name;
            if (assets != null) {
                this.assets.addAll(Arrays.asList(assets));
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Human human = (Human) o;

            if (name != null ? !name.equals(human.name) : human.name != null) return false;
            return assets != null ? assets.equals(human.assets) : human.assets == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (assets != null ? assets.hashCode() : 0);
            return result;
        }

        public void save(OutputStream outputStream) throws Exception {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            String isNameEmpty = name.isEmpty() ? "yes" : "no";
            String isAssetsListEmpty = assets.isEmpty() ? "yes" : "no";

            writer.write(isNameEmpty + "\n");
            writer.write(isAssetsListEmpty + "\n");

            if (!name.isEmpty()) {
                writer.write(name + "\n");
            }
            if (!assets.isEmpty()) {
                writer.write(assets.size() + "\n");
                for (Asset asset : assets) {
                    writer.write(asset.getName() + "\n");
                    writer.write(asset.getPrice() + "\n");
                }
            }
            writer.close();
        }

        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String isNameEmpty = reader.readLine();
            String isAssetsListEmpty = reader.readLine();

            if (isNameEmpty.equals("no")) {
                this.name = reader.readLine();
            }
            if (isAssetsListEmpty.equals("no")) {
                int assetsCount = Integer.parseInt(reader.readLine());
                for (int i = 0; i < assetsCount; i++) {
                    assets.add(new Asset(reader.readLine(), Double.parseDouble(reader.readLine())));
                }
            }
            reader.close();
        }
    }
}
