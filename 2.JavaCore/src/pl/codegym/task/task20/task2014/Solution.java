package pl.codegym.task.task20.task2014;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/* 
Serializowane rozwiązanie
*/

public class Solution implements Serializable {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Solution savedObject = new Solution(28);
        System.out.println(savedObject);

        String filename = "test.class";

        OutputStream outputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(savedObject);
        objectOutputStream.close();
        outputStream.close();

        InputStream inputStream = new FileInputStream(filename);
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        Object loadedObject = objectInputStream.readObject();
        objectInputStream.close();
        inputStream.close();

        Solution newSolution = new Solution(33);
        System.out.println(savedObject.toString().equals(loadedObject.toString()));
        System.out.println(newSolution);
    }

    private transient final String pattern = "dd MMMM yyyy, EEEE";
    private transient Date currentDate;
    private transient int temperature;
    String string;

    public Solution(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Dziś jest %s, aktualna temperatura wynosi %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);
    }

    @Override
    public String toString() {
        return this.string;
    }
}
