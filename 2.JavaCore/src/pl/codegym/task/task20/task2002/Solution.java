package pl.codegym.task.task20.task2002;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/* 
Odczytywanie i zapisywanie do pliku: CodeGym
*/

public class Solution {

    public static void main(String[] args) {
        // Plik your_file_name.tmp znajdziesz w swoim folderze plików tymczasowych,
        // ewentualnie dostosuj outputStream/inputStream do rzeczywistej lokalizacji pliku
        try {
            File yourFile = File.createTempFile("your_file_name", null);
//            File yourFile = new File("your_file_name");
            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            CodeGym codeGym = new CodeGym();
            // Inicjalizuj tutaj pole users dla obiektu codeGym
            User firstUser = new User();
            firstUser.setFirstName("Mary");
            firstUser.setLastName("Smith");
            firstUser.setBirthDate(new Date("1-MAY-1980"));
            firstUser.setMale(false);
            firstUser.setCountry(User.Country.UNITED_KINGDOM);
            codeGym.users.add(firstUser);

            User secondUser = new User();
            secondUser.setFirstName("Tony");
            secondUser.setLastName("Robson");
            secondUser.setBirthDate(new Date("31-DEC-2000"));
            secondUser.setMale(true);
            secondUser.setCountry(User.Country.UNITED_STATES);
            codeGym.users.add(secondUser);

            codeGym.save(outputStream);
            outputStream.flush();

            CodeGym loadedObject = new CodeGym();
            loadedObject.load(inputStream);

            // Sprawdź tutaj, czy obiekt codeGym jest taki sam jak obiekt loadedObject
            System.out.println(codeGym.equals(loadedObject) ? "Same" : "Different");

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, coś jest nie tak z moim plikiem");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Oops, coś jest nie tak z metodą save/load");
        }
    }

    public static class CodeGym {

        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            String isUserListEmpty = users.isEmpty() ? "yes" : "no";
            writer.write(isUserListEmpty + "\n");

            if (!users.isEmpty()) {
                writer.write(users.size() + "\n");
                for (User user : users) {
                    writer.write(user.getFirstName() + "\n");
                    writer.write(user.getLastName() + "\n");
                    writer.write(user.getBirthDate().toGMTString() + "\n");
                    writer.write((user.isMale() ? "male" : "female") + "\n");
                    writer.write(user.getCountry() + "\n");
                }
            }
            writer.close();
        }

        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String isUserListEmpty = reader.readLine();

            if (isUserListEmpty.equals("no")) {
                int usersEntries = Integer.parseInt(reader.readLine());
                for (int i = 0; i < usersEntries; i++) {
                    User user = new User();
                    user.setFirstName(reader.readLine());
                    user.setLastName(reader.readLine());
                    user.setBirthDate(new Date(reader.readLine()));
                    user.setMale(reader.readLine().equals("male"));
                    user.setCountry(User.Country.valueOf(reader.readLine()));
                    users.add(user);
                }
            }

            reader.close();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CodeGym codeGym = (CodeGym) o;

            return Objects.equals(users, codeGym.users);
        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }
    }
}
