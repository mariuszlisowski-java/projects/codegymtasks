package pl.codegym.task.task20.task2003;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/* 
Wprowadzenie do properties
*/

public class Solution {

    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() throws Exception {
        load(new FileInputStream(getFilename()));
    }

    private String getFilename() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
//        String filename = "test.properties";
        reader.close();

        return filename;
    }

    public void save(OutputStream outputStream) throws Exception {
        Properties props = new Properties();

        for (Map.Entry<String, String> property : properties.entrySet()) {
            props.setProperty(property.getKey(), property.getValue());
        }
        props.store(outputStream, "no comments");
    }

    public void load(InputStream inputStream) throws Exception {
        Properties props = new Properties();
        props.load(inputStream);

        for (Map.Entry<Object, Object> property : props.entrySet()) {
            properties.put((String) property.getKey(), (String) property.getValue());
        }
    }

    public static void main(String[] args) throws Exception {
        Solution solution = new Solution();
        solution.fillInPropertiesMap();
        solution.save(new FileOutputStream("output.properties"));

    }
}
