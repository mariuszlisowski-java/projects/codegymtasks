package pl.codegym.task.task18.task1827;

/* 
Ceny
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        String productName;
        String price;
        String quantity;
        if (args.length == 4 && args[0].equals("-c")) {
            productName =  args[1];
            price = args[2];
            quantity = args[3];
        } else {
            System.out.println("Any arguments?");
            return;
        }
        int maxIdInFile = findMaxIdInFile(filename);
        appendToFile(maxIdInFile ,productName, price, quantity, filename);
    }

    private static int findMaxIdInFile(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line;
        int maxId = 0;
        while ((line = reader.readLine()) != null) {
            int id = Integer.parseInt(line.substring(0, 8).trim()); // [0, 7]
            if (id > maxId) {
                maxId = id;
            }
        }
        reader.close();

        return maxId;
    }
    private static void appendToFile(int id, String productName, String price, String quantity,
                                     String filename) throws IOException {
        BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(filename, true));

        StringBuilder builder = new StringBuilder();
        byte[] buffer = builder
                .append(rightPadSpaces(String.valueOf(++id), 8))
                .append(rightPadSpaces(productName,30))
                .append(rightPadSpaces(price, 8))
                .append(rightPadSpaces(quantity, 4))
                .toString()
                .getBytes();
        output.write(0x0a); // line feed
        output.write(buffer);
        output.close();
    }

    private static String rightPadSpaces(String str, int maxLength) {
        return String.format("%1$-" + maxLength + "s", str);
    }
}
// id            8 znaków
// productName  30 znaków
// price         8 znaków
// quantity      4 znaki
//
// "19847   Swim trunks, blue             159.00  12"
//
// -c "SWIM trunks, blue" "999.00" "99"
// -c "Snowboard jacket with reflecti" 10173.99 1234
