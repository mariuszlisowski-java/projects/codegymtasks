package pl.codegym.task.task18.task1822;

/* 
Wyszukiwanie danych wewnątrz pliku
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("Pass ID as an argument!");
            return;
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        reader.close();

        try {
            readAndSearchFile(filename, args[0]);
        } catch (IOException exception) {
            System.out.println("File opening error!");
        }
    }

    private static void readAndSearchFile(String filename, String id) throws IOException {
        InputStream inputStream = new FileInputStream(filename);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] words = line.split(" ");
            if (words[0].equals(id)) {
                System.out.println(line);
                break;
            }
        }

        bufferedReader.close();
        inputStreamReader.close();
        inputStream.close();
    }

}
