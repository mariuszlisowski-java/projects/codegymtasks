package pl.codegym.task.task18.task1824;

/* 
Pliki i wyjątki
*/

import java.io.*;

public class Solution {

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        final int ARRAY_SIZE = 1000;
        String[] filenames = new String[ARRAY_SIZE];
        InputStream[] inputStreams = new InputStream[ARRAY_SIZE];
        int index = 0;
        while (true) {
            try {
                filenames[index] = reader.readLine();
                inputStreams[index] = new FileInputStream(filenames[index]);
                ++index;
            } catch (FileNotFoundException e) {
                System.out.println(filenames[index]);
                break;
            } catch (IOException e) {
                break;
            } finally {
                for (int i = 0; i < ARRAY_SIZE; i++) {
                    try {
                        if (inputStreams[i] != null) {
                            inputStreams[i].close();
                        }
                    } catch (IOException e) {
                        break;
                    }
                }
            } // finally

        } // while

    }

}
