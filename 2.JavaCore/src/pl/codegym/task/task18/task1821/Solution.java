package pl.codegym.task.task18.task1821;

/* 
Częstotliwość występowania symboli
*/

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) {
        byte[] bytes = null;
        if (args.length > 0) {
            String filename = args[0];
            bytes = readFile(filename);
        } else {
            System.out.println("No filename passed! Exitting...");
            return;
        }
        if (bytes == null) {
            return;
        }

        Map<Character, Integer> charsCount = new TreeMap<>();

        for (byte aByte : bytes) {
                Character character = (char) (aByte & 0xff);            // all ASCII [0, 255]
                charsCount.merge(character, 1, Integer::sum);
        }

        for (Map.Entry<Character, Integer> map : charsCount.entrySet()) {
            System.out.println(map.getKey() + " " + map.getValue());
        }

    }

    private static byte[] readFile(String filename) {
        byte[] buffer = null;
        try (InputStream inputStream = new FileInputStream(filename);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream))
        {
            buffer = new byte[inputStream.available()];
            bufferedInputStream.read(buffer);
        } catch (IOException exception) {
            System.out.println("File opening error!");
        }

        return buffer;
    }

}
