package pl.codegym.task.task18.task1823;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Wątki i bajty
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String filename;
        while (!(filename = reader.readLine()).equals("exit")) {
            new ReadThread(filename).start();
        }
    }

    public static class ReadThread extends Thread {
        String filename;

        public ReadThread(String filename) {
            this.filename = filename;
        }

        @Override
        public void run() {
            byte[] buffer;
            try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(filename)))
            {
                buffer = new byte[input.available()];
                input.read(buffer);
            } catch (FileNotFoundException e) {
                System.out.println(getName() + ": file not found!");
                return;
            } catch (IOException e) {
                System.out.println(getName() + ": file opening error!");
                return;
            }
            System.out.println(getName() + " started... file read correctly");

            Map<Integer, Integer> bytesMap = new HashMap<>();
            for (byte aByte : buffer) {
                bytesMap.merge(aByte & 0xff, 1, Integer::sum);
            }

            int max = 0;
            int key = 0;
            for (Map.Entry<Integer, Integer> map : bytesMap.entrySet()) {
                if (map.getValue() > max) {
                    max = map.getValue();
                    key = map.getKey();
                }
            }

            resultMap.put(filename, key);
        }

    }
}
