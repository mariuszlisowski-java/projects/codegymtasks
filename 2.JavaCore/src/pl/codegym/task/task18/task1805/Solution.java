package pl.codegym.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/* 
Sortowanie bajtów
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Set<Integer> uniques = new TreeSet<>();

        String filename = reader.readLine();
        InputStream inputStream = new FileInputStream(filename);

        while (inputStream.available() > 0) {
            uniques.add(inputStream.read());
        }
        inputStream.close();

        for (Integer value : uniques) {
            System.out.print(value + " ");
        }
    }

}
