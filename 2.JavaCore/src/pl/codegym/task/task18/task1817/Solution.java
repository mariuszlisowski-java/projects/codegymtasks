package pl.codegym.task.task18.task1817;

/* 
Spacje
*/

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

public class Solution {
    public static void main(String[] args) {
        String filename;
        if (args.length > 0) {
            filename = args[0];
        } else {
            return;
        }

        byte[] buffer;
        try (InputStream inputStream = new FileInputStream(filename);
             BufferedInputStream bufferedInput = new BufferedInputStream(inputStream))
        {
            buffer = new byte[bufferedInput.available()];
            bufferedInput.read(buffer);
        } catch (IOException e) {
            System.out.println("File read error!");
            return;
        }

        int spacesCounter = 0;
        for (byte b : buffer) {
            if (b == 32) {
                ++spacesCounter;
            }
        }
        DecimalFormat decimal = new DecimalFormat("#.##");
        double percentage = (1.0 * spacesCounter) / buffer.length * 100.0;

        System.out.println(decimal.format(percentage));
    }
}
