package pl.codegym.task.task18.task1801;

import java.io.*;

/* 
Maksymalna liczba bajtów
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        InputStream inputStream = new FileInputStream(filename);
        int max = 0;
        while (inputStream.available() > 0) {
            int current = inputStream.read();
            if (current > max) {
                max = current;
            }
        }
        System.out.println(max);
        inputStream.close();
    }
}
