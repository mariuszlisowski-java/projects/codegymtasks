package pl.codegym.task.task18.task1803;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

/* 
Najczęściej występujące bajty
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Map<Integer, Integer> counts = new HashMap<>();

        String filename = reader.readLine();
        InputStream inputStream = new FileInputStream(filename);

        while (inputStream.available() > 0) {
            counts.merge(inputStream.read(), 1, Integer::sum);
        }
        inputStream.close();

        int maxValue = 0;
        for (Integer value : counts.values()) {
            if (value > maxValue) {
                maxValue = value;
            }
        }

        Set<Integer> maxOccurences = getKeysByValue(counts, maxValue);

        for (Integer value : maxOccurences) {
            System.out.print(value + " ");
        }

    }

    private static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
        Set<T> keys = new HashSet<>();

        for (Map.Entry<T, E> pair : map.entrySet()) {
            if (pair.getValue().equals(value)) {
                keys.add(pair.getKey());
            }
        }

        return keys;
    }

}
