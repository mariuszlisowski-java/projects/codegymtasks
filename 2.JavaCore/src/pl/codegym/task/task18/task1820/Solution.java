package pl.codegym.task.task18.task1820;

/*
Zaokrąglanie liczb
*/

import java.io.*;
import java.text.ParseException;

// 3.49 => 3    // -3.49 => -3  // 3.50 => 4    // -3.50 => -3  // 3.51 => 4    // -3.51 => -4
// 3.49 -3.49 3.50 -3.50 3.51 -3.51

public class Solution {
    private static final byte ASCII_ZERO = 48;
    private static final byte ASCII_NINE = 57;
    private static final byte ASCII_DOT = 46;
    private static final byte ASCII_SPACE = 32;
    private static final byte ASCII_MINUS = 45;
    private static final byte ASCII_LINE_FEED = 10;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
//        String filename1 = "test.nums";
//        String filename2 = "output.nums";

        try (InputStream inputStream = new FileInputStream(filename1);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

             OutputStream outputStream = new FileOutputStream(filename2);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream))
        {
            byte[] buffer = new byte[inputStream.available()];
            bufferedInputStream.read(buffer);

            byte[] rounded;
            try {
                rounded = roundUpNumbers(buffer);
            } catch (ParseException e) {
                System.out.println("File parsing error!");
                return;
            }

            bufferedOutputStream.write(rounded);
        } catch (IOException e) {
            System.out.println("File read error!");
        }



    }
    private static byte[] roundUpNumbers(byte[] bytes) throws ParseException {
        StringBuilder numberBuilder = new StringBuilder();
        byte[] result = new byte[bytes.length];
        int index = 0;
        for (int i = 0; i < bytes.length; i++) {
            if (bytes[i] == ASCII_SPACE || bytes[i] == ASCII_LINE_FEED) {
                index = appendToResult(numberBuilder, result, index);

                if (bytes[i] == ASCII_LINE_FEED) {
                    result[index] = ASCII_LINE_FEED;
                    break;
                } else {
                    numberBuilder.setLength(0);
                    continue;
                }
            }

            if ((bytes[i] >= ASCII_ZERO && bytes[i] <= ASCII_NINE) || bytes[i] == ASCII_DOT || bytes[i] == ASCII_MINUS) {
                numberBuilder.append((char) bytes[i]);
                if (i == bytes.length -1) {
                    appendToResult(numberBuilder, result, index);
                }
            } else {
                throw new ParseException("Not a digit or a digital dot!", bytes[i]);
            }
        }

        return result;
    }

    private static int appendToResult(StringBuilder numberBuilder, byte[] result, int index) {
        double number = Double.parseDouble(numberBuilder.toString());
        long roundedNumber = Math.round(number);
        String numberString = String.valueOf(roundedNumber);
        byte[] bytesNumber = numberString.getBytes();

        index = concatArray(result, index, bytesNumber);
        return index;
    }

    private static int concatArray(byte[] result, int index, byte[] bytesNumber) {
        for (byte aByte : bytesNumber) {
            result[index++] = aByte;
        }
        result[index++] = ASCII_SPACE;

        return index;
    }


}
