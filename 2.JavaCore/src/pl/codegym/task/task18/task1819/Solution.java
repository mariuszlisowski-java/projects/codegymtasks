package pl.codegym.task.task18.task1819;

/* 
Łączenie plików
*/

import java.io.*;

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();

        byte[] temp;
        try (InputStream inputStream = new FileInputStream(filename1);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream))
        {
            temp = new byte[bufferedInputStream.available()];
            bufferedInputStream.read(temp);
        } catch (IOException e) {
            return;
        }

        try (InputStream inputStream = new FileInputStream(filename2);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
             OutputStream outputStream = new FileOutputStream(filename1);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream))
        {
            byte[] buffer = new byte[bufferedInputStream.available()];
            bufferedInputStream.read(buffer);
            bufferedOutputStream.write(buffer);
        } catch (IOException e) {
            return;
        }

        try (OutputStream outputStream = new FileOutputStream(filename1, true);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream))
        {
            bufferedOutputStream.write(temp);
        } catch (IOException e) {
            return;
        }
    }

}
// temp = 1st
// 1st = 2nd    // replace
// 1st += temp  // append
