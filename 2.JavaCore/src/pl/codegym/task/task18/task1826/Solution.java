package pl.codegym.task.task18.task1826;

/* 
Szyfrowanie
*/

import java.io.*;

public class Solution {
    
    public static void main(String[] args) {
        try {
            readArguments(args);
        } catch (InvalidArgumentNumberException e) {
            System.out.println("Usage: cipher (-e | -d) inputFilename outputFilename");
        }
    }

    private static void readArguments(String[] args) throws InvalidArgumentNumberException {
        if (args.length > 0 & args.length <= 3) {
            String command = args[0];
            if (command.equals("-e")) {
                crypt(args[1], args[2], Cipher.ENCRYPT);
            } else if (command.equals("-d")) { 
                crypt(args[1], args[2], Cipher.DECRYPT);
            }
        }
        throw new InvalidArgumentNumberException();
    }

    private static void crypt(String inputFilename, String outputFilename, Cipher cipher) {
        try (
            InputStream inputStream = new FileInputStream(inputFilename);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

            OutputStream outputStream = new FileOutputStream(outputFilename);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream))
        {
            byte[] buffer = new byte[bufferedInputStream.available()];
            bufferedInputStream.read(buffer);
            if (cipher.equals(Cipher.ENCRYPT)) {
                 encrypt(buffer);
            } else
            if (cipher.equals(Cipher.DECRYPT)) {
                 decrypt(buffer);
            }
            bufferedOutputStream.write(buffer);
        } catch (IOException e) {}

    }

    private static void encrypt(byte[] buffer) {
        // stub for encrypt
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] ^= 1;
        }
    }

    private static void decrypt(byte[] buffer) {
        // stub for decrypt
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] ^= 1;
        }
    }

    private enum Cipher {
        ENCRYPT, DECRYPT
    }

    private static class InvalidArgumentNumberException extends Throwable {}
}
