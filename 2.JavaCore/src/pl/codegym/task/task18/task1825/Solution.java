package pl.codegym.task.task18.task1825;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/* 
Tworzenie pliku
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> filenames = new ArrayList<>();

        String filename;
        while (!(filename = reader.readLine()).equals("end")) {
            filenames.add(filename);
        }
        reader.close();
        Collections.sort(filenames);

        String regex = "(.part\\d+)";
        String outputFilename = filenames.get(0).replaceAll(regex, "");

        mergeFiles(filenames, outputFilename);
    }

    private static void mergeFiles(List<String> filenames, String outputFilename) throws IOException {
        BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(outputFilename, true));
        BufferedInputStream input = null;

        for (String filename : filenames) {
            input = new BufferedInputStream(new FileInputStream(filename));
            byte[] buffer = new byte[input.available()];
            input.read(buffer);
            input.close();
            output.write(buffer);
        }
        output.close();
    }

}
