package pl.codegym.task.task18.task1809;

/* 
Odwracanie pliku
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();

        InputStream inputStream = new FileInputStream(filename1);
        OutputStream outputStream = new FileOutputStream(filename2);

        if (inputStream.available() > 0) {
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);

            reverse(buffer);

            outputStream.write(buffer);
        }

        inputStream.close();
        outputStream.close();
    }

    public static void reverse(byte[] array) {
        for(int i = 0; i < array.length / 2; i++) {
            byte temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }
    }
}
