package pl.codegym.task.task18.task1807;

/* 
Liczenie przecinków
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        InputStream inputStream = new FileInputStream(filename);
        byte[] buffer = new byte[inputStream.available()];
        inputStream.read(buffer);
        inputStream.close();

        int count = 0;
        for (byte value : buffer) {
            if (value == (byte) ',') {
                ++count;
            }
        }

        System.out.println(count);
    }
}
