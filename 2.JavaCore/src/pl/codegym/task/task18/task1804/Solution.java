package pl.codegym.task.task18.task1804;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* 
Najrzadziej występujące bajty
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Map<Integer, Integer> counts = new HashMap<>();

        String filename = reader.readLine();
        InputStream inputStream = new FileInputStream(filename);

        while (inputStream.available() > 0) {
            counts.merge(inputStream.read(), 1, Integer::sum);
        }
        inputStream.close();

        int minValue = Integer.MAX_VALUE;
        for (Integer value : counts.values()) {
            if (value < minValue) {
                minValue = value;
            }
        }

        Set<Integer> minOccurences = getKeysByValue(counts, minValue);

        for (Integer value : minOccurences) {
            System.out.print(value + " ");
        }
    }

    private static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
        Set<T> keys = new HashSet<>();

        for (Map.Entry<T, E> pair : map.entrySet()) {
            if (pair.getValue().equals(value)) {
                keys.add(pair.getKey());
            }
        }

        return keys;
    }

}
