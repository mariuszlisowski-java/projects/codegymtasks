package pl.codegym.task.task18.task1828;

/* 
Ceny 2
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        String id;
        String productName;
        String price;
        String quantity;
        if (args.length == 5 && args[0].equals("-u")) {
            id = args[1];
            productName =  args[2];
            price = args[3];
            quantity = args[4];
            updateLineWithId(id, productName, price, quantity, filename);
        } else if (args.length == 2 && args[0].equals("-d")) {
            id = args[1];
            deleteLineWithId(id, filename);
        } else {
            System.out.println("Any arguments?");
            return;
        }
    }

    private static void deleteLineWithId(String id, String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        List<String> lines = new ArrayList<>();
        String line;

        // store all lines except one containing an id
        while ((line = reader.readLine()) != null) {
            if (!line.substring(0, 8).trim().equals(id)) {
                lines.add(line);
            }
        }
        reader.close();

        // write all remaining lines
        BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(filename));
        for (String str : lines) {
            output.write(str.getBytes());
            output.write(0x0a);         // add line feed
        }
        output.close();
    }

    private static void updateLineWithId(String id, String productName, String price, String quantity,
                                         String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        List<String> lines = new ArrayList<>();
        String line;

        // store all lines updating one with this id
        while ((line = reader.readLine()) != null) {
            if (!line.substring(0, 8).trim().equals(id)) {
                lines.add(line);
            } else {
                StringBuilder builder = new StringBuilder();
                line = builder
                        .append(rightPadSpaces(id, 8))
                        .append(rightPadSpaces(productName, 30))
                        .append(rightPadSpaces(price, 8))
                        .append(rightPadSpaces(quantity, 4))
                        .toString();
                lines.add(line);
            }
        }
        reader.close();

        BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(filename));
        for (String str : lines) {
            output.write(str.getBytes());
            output.write(0x0a);         // add line feed
        }
        output.close();
    }

    private static String rightPadSpaces(String str, int maxLength) {
        return String.format("%1$-" + maxLength + "s", str);
    }

}
// id            8 znaków
// productName  30 znaków
// price         8 znaków
// quantity      4 znaki
//
// "19847   Swim trunks, blue             159.00  12"
//
// -d 19847
// -d 198479
// -d 19847983
//
// -u 19847 "Swim trunks, blue" 159.00 12
// -u 198479  "Swim trunks, black with printe" 173.00 17
// -u 19847983 "Snowboard jacket with reflecti" 10173.99 1234