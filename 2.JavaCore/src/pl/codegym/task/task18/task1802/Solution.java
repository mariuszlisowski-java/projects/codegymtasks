package pl.codegym.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

/* 
Minimalna liczba bajtów
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        InputStream inputStream = new FileInputStream(filename);
        int min = Integer.MAX_VALUE;
        while (inputStream.available() > 0) {
            int current = inputStream.read();
            if (current < min) {
                min = current;
            }
        }
        System.out.println(min);
        inputStream.close();
    }
}
