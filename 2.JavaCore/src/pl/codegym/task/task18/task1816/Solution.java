package pl.codegym.task.task18.task1816;

/* 
ABC
*/

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Solution {

    public static void main(String[] args) {
        final short MIN_LETTER_UPPERCASE = 65;
        final short MAX_LETTER_UPPERCASE = 90;
        final short MIN_LETTER_LOWERCASE = 97;
        final short MAX_LETTER_LOWERCASE = 122;
        String filename;
        if (args.length > 0) {
            filename = args[0];
        } else {
            return;
        }

        byte[] buffer;
        try (InputStream inputStream = new FileInputStream(filename);
             BufferedInputStream bufferedInput = new BufferedInputStream(inputStream))
        {
            buffer = new byte[bufferedInput.available()];
            while (bufferedInput.available() > 0) {
                bufferedInput.read(buffer);
            }
        } catch (IOException e) {
            System.out.println("File read error!");
            return;
        }

        int letterCount = 0;
        for (byte b : buffer) {
            if ((b >= MIN_LETTER_UPPERCASE && b <= MAX_LETTER_UPPERCASE) ||
                (b >= MIN_LETTER_LOWERCASE && b <= MAX_LETTER_LOWERCASE))
            {
                ++letterCount;
            }
        }

        System.out.println(letterCount);
    }
}
