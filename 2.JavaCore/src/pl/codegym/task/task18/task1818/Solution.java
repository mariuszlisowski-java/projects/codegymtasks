package pl.codegym.task.task18.task1818;

/* 
Dwa w jednym
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
        String filename3 = reader.readLine();


        try (OutputStream outputStream = new FileOutputStream(filename1, true);
             InputStream inputStream2 = new FileInputStream(filename2);
             InputStream inputStream3 = new FileInputStream(filename3))
        {
            byte[] buffer = new byte[inputStream2.available()];
            inputStream2.read(buffer);
            outputStream.write(buffer);

            buffer = new byte[inputStream3.available()];
            inputStream3.read(buffer);
            outputStream.write(buffer);
        } catch (IOException e) {}

    }

}
