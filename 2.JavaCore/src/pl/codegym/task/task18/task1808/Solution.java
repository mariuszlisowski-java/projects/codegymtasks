package pl.codegym.task.task18.task1808;

/* 
Dzielenie pliku
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
        String filename3 = reader.readLine();

        InputStream inputStream = new FileInputStream(filename1);
        OutputStream outputStreamA = new FileOutputStream(filename2);
        OutputStream outputStreamB = new FileOutputStream(filename3);

        byte[] bufferB = new byte[inputStream.available() / 2];
        byte[] bufferA;
        if (inputStream.available() % 2 == 0) {
            bufferA = new byte[inputStream.available() / 2];        // even file length
        } else {
            bufferA = new byte[inputStream.available() / 2 + 1];    // odd file length
        }

        if (inputStream.available() > 0) {
            inputStream.read(bufferA);
            outputStreamA.write(bufferA);

            inputStream.read(bufferB);
            outputStreamB.write(bufferB);
        }

        inputStream.close();
        outputStreamA.close();
        outputStreamB.close();
    }
}
