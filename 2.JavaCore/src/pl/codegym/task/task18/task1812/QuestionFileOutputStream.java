package pl.codegym.task.task18.task1812;

import java.io.*;
import java.util.Scanner;

/* 
Rozszerzanie AmigoOutputStream
*/

public class QuestionFileOutputStream implements AmigoOutputStream {
    private AmigoOutputStream outputStream;

    public QuestionFileOutputStream(AmigoOutputStream amigoOutputStream) {
        outputStream = amigoOutputStream;
    }

    @Override
    public void flush() throws IOException {
        outputStream.flush();
    }

    @Override
    public void write(int b) throws IOException {
        outputStream.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        outputStream.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        outputStream.write(b, off ,len);
    }

    @Override
    public void close() throws IOException {
        System.out.println("Czy naprawdę chcesz zamknąć strumień? T/N");
        Scanner scanner = new Scanner(System.in);
        if (scanner.nextLine().equals("T")) {
            outputStream.close();
        }
        scanner.close();
    }
}

