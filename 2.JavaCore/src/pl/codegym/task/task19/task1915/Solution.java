package pl.codegym.task.task19.task1915;

/* 
Powielanie tekstu
*/

import java.io.*;

public class Solution {

    public static TestString testString = new TestString();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        reader.close();

        PrintStream console = System.out;

        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(arrayOutputStream);
        System.setOut(printStream);

        testString.printSomething();

        OutputStream outputStream = new FileOutputStream(filename);
        outputStream.write(arrayOutputStream.toByteArray());
        outputStream.close();

        System.setOut(console);
        System.out.println(arrayOutputStream);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("To jest tekst na potrzeby testów");
        }
    }
}

