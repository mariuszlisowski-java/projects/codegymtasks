package pl.codegym.task.task19.task1927;

/* 
Reklama kontekstowa
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {

    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream console = System.out;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);

        System.setOut(printStream);
        testString.printSomething(); // printing to outputStream

        System.setOut(console);
        printAd(outputStream);
    }

    private static void printAd(ByteArrayOutputStream outputStream) {
        String[] lines = outputStream.toString().split("\n");
        for (int i = 0; i < lines.length; i++) {
            System.out.println(lines[i]);
            if (i % 2 == 1) {
                System.out.println("CodeGym - kursy Java online");
            }
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("pierwszy");
            System.out.println("drugi");
            System.out.println("trzeci");
            System.out.println("czwarty");
            System.out.println("piąty");
        }
    }
}
