package pl.codegym.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
John Johnson
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<>();

    public static void main(String[] args) {
        String filename;
        if (args.length == 1) {
            filename = args[0];
        } else {
            return;
        }

        List<String> lines = readFile(filename);
        parseLinesAndCreatePersons(lines);
//        printPeople(PEOPLE);
    }

    private static void printPeople(List<Person> people) {
        for (Person person : people) {
            System.out.println(person.getName());
            System.out.println(person.getBirthDate());
        }
    }

    private static void parseLinesAndCreatePersons(List<String> lines) {
        StringBuilder nameBuilder = new StringBuilder();
        StringBuilder dateBuilder = new StringBuilder(); // month day year

        for (String line : lines) {
            String[] words = line.split(" ");
            Iterator<String> iterator = Arrays.stream(words).iterator();
            String word;
            // build name (may contain a dash) and date (with dashes)
            while (iterator.hasNext()) {
                word = iterator.next();
                if (word.matches("([A-Za-z-]+)")) {
                    nameBuilder.append(word).append(" ");
                } else {
                    dateBuilder.append(word).append("-");
                }
            }
            nameBuilder.deleteCharAt(nameBuilder.length() - 1); // delete last space
            dateBuilder.deleteCharAt(dateBuilder.length() - 1); // delete last dash

            createPerson(nameBuilder.toString(), dateBuilder.toString());

            // reuse the builders
            nameBuilder.setLength(0);
            dateBuilder.setLength(0);
        }
    }

    private static void createPerson(String name, String date) {
        PEOPLE.add(new Person(name, getFormattedDate(date)));
    }

    private static Date getFormattedDate(String date) {
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        Date formattedDate = null;
        try {
            formattedDate = dateFormat.parse(date);
        } catch (ParseException e) {
            System.out.println("Date parsing error!");
        }

        return formattedDate;
    }

    private static List<String> readFile(String filename) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException exception) { }

        return lines;
    }
}
