package pl.codegym.task.task19.task1924;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.function.Consumer;

/* 
Zastępowanie liczb
*/

public class Solution {
    public static Map<Integer, String> map = new HashMap<Integer, String>();

    static {
        map.put(0, "zero");
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
        map.put(8, "eight");
        map.put(9, "nine");
        map.put(10, "ten");
        map.put(11, "eleven");
        map.put(12, "twelve");
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        reader.close();

        List<String> lines = readFile(filename);
        replaceNumbersWithWords(lines);
        printList(lines);
    }

    private static void printList(List<String> lines) {
        lines.stream()
                .forEach(System.out::println);
    }

    private static void replaceNumbersWithWords(List<String> lines) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < lines.size(); i++) {
            // split each line into separate words
            String[] words = lines.get(i).split(" ");

            for (String word : words) {
                String finalWord;
                boolean dotPresent = false;
                boolean commaPresent = false;
                if (word.endsWith(".")) {
                    finalWord = word.replaceFirst("(.$)", "");  // remove last dot
                    dotPresent = true;
                } else if (word.endsWith(",")) {
                    finalWord = word.replaceFirst("(.$)", "");  // remove last comma
                    commaPresent = true;
                } else {
                    finalWord = word;
                }

                Optional<String> mappedNumber = map.entrySet().stream()
                        .filter(entry -> finalWord.equals(String.valueOf(entry.getKey())))
                        .map(Map.Entry::getValue)
                        .findFirst();

                boolean finalDotPresent = dotPresent;
                boolean finalCommaPresent = commaPresent;

                mappedNumber.ifPresent(string -> {
                        if (finalDotPresent) {
                            builder.append(string).append(".");
                        } else if (finalCommaPresent) {
                            builder.append(string).append(", ");
                        } else {
                            builder.append(string).append(" ");
                        }
                    });
                if ((mappedNumber.orElse("")).equals("")) {
                    builder.append(word).append(" ");
                }

//                mappedNumber.ifPresentOrElse(string -> {
//                        if (finalDotPresent) {
//                            builder.append(string).append(".");
//                        } else if (finalCommaPresent) {
//                            builder.append(string).append(", ");
//                        } else {
//                            builder.append(string).append(" ");
//                        }
//                    },
//                    () -> builder.append(word).append(" "));
            }

            lines.set(i, builder.toString());
            builder.setLength(0);
        }

    }

    private static List<String> readFile(String filename) {
        List<String> lines = new ArrayList<>();
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }

        } catch (IOException exception) {}

        return lines;
    }

}
