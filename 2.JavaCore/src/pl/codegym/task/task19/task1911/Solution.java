package pl.codegym.task.task19.task1911;

/* 
Reader wrapper
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {

    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream console = System.out;

        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(arrayOutputStream);

        System.setOut(printStream);

        testString.printSomething();
        String uppercaseString = arrayOutputStream.toString().toUpperCase();

        System.setOut(console);

        System.out.println(uppercaseString);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("To jest tekst na potrzeby testów");
        }
    }
}
