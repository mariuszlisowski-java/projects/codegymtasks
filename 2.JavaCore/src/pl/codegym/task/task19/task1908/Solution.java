package pl.codegym.task.task19.task1908;

/* 
Wybieranie numerów
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputFilename = reader.readLine();
        String outputFilename = reader.readLine();

        extractedAndWriteNumbersFromFile(inputFilename, outputFilename);

        reader.close();
    }

    private static void extractedAndWriteNumbersFromFile(String inFile, String outFile) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(inFile));
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outFile));

        String line;
        while ((line = fileReader.readLine()) != null) {
            String lineOfNumbers = extractNumbersFromLine(line);
            fileWriter.write(lineOfNumbers + "\n");
        }

        fileReader.close();
        fileWriter.close();
    }

    private static String extractNumbersFromLine(String line) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] expressions = line.split(" ");
        for (String expression : expressions) {
            try {
                int number = Integer.parseInt(expression);
                stringBuilder.append(number)
                             .append(" ");
            } catch (NumberFormatException exception) {
                // no-op
            }
        }

        return stringBuilder.toString();
    }
}
