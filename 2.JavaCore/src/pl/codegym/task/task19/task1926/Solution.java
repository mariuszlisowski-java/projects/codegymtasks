package pl.codegym.task.task19.task1926;

/* 
Lustrzane odbicie
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        reader.close();

        List<String> content = readFile(filename);
        reverseEachLine(content);
        printList(content);
    }

    private static void printList(List<String> content) {
        content.stream()
                .forEach(System.out::println);
    }

    private static void reverseEachLine(List<String> list) {
        int index = 0;
        for (String string : list) {
            list.set(index++, new StringBuilder(string).reverse().toString());
        }
    }

    private static List<String> readFile(String filename) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException exception) {
            System.out.println("File read error!");
        }

        return lines;
    }
}
