package pl.codegym.task.task19.task1923;

/* 
Słowa z liczbami
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        String file1;
        String file2;
        if (args.length == 2) {
            file1 = args[0];
            file2 = args[1];
        } else {
            return;
        }

        List<String> lines = readFile(file1);
        String result = selectWordsWithNumbers(lines);
        System.out.println(result);
        writeFile(file2, result);
    }

    private static String selectWordsWithNumbers(List<String> lines) {
        StringBuilder builder = new StringBuilder();
        for (String line : lines) {
            String[] words = line.split(" ");
            for (String word : words) {
                if (word.matches("(.*\\d+.*)")) {
                    builder.append(word).append(" ");
                }
            }
        }

        return builder.toString();
    }

    private static void writeFile(String filename, String content) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            writer.write(content);
        } catch (IOException exception) {
            System.out.println("Fire writing error!");
        }
    }

    private static List<String> readFile(String filename) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException exception) {
            System.out.println("File opening error!");
        }

        return lines;
    }

}
