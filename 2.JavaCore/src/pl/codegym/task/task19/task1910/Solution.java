package pl.codegym.task.task19.task1910;

/* 
Interpunkcja
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputFilename = reader.readLine();
        String outputFilename = reader.readLine();

        writeFileWithoutPunctuationMarks(inputFilename, outputFilename);

        reader.close();
    }

    private static void writeFileWithoutPunctuationMarks(String inFilename, String outFilename) {
        try (BufferedReader fileReader = new BufferedReader(new FileReader(inFilename));
             BufferedWriter fileWriter = new BufferedWriter(new FileWriter(FileDescriptor.out)))
        {
            String line;
            while ((line = fileReader.readLine()) != null) {
                String lineWithoutPunctuationMarks = removeAllPunctuationMarks(line);
                fileWriter.write(lineWithoutPunctuationMarks);
            }
        } catch (IOException exception) {
            System.out.println("IO error " + exception.getMessage());
        }
    }


    private static String removeAllPunctuationMarks(String line) {
        return line.replaceAll("\\p{Punct}+", "");
    }
}
