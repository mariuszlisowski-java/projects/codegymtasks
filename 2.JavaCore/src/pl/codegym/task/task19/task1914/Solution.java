package pl.codegym.task.task19.task1914;

/* 
Rozwiązywanie problemów
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream console = System.out;

        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(arrayOutputStream);
        System.setOut(printStream);

        testString.printSomething();
        String expression = arrayOutputStream.toString();
        String[] words = arrayOutputStream.toString().split(" ");
        int result = 0;
        if (expression.contains("+")) {
            result = Integer.parseInt(words[0]) + Integer.parseInt(words[2]);
        } else if (expression.contains("-")) {
            result = Integer.parseInt(words[0]) - Integer.parseInt(words[2]);
        } else if (expression.contains("*")) {
            result = Integer.parseInt(words[0]) * Integer.parseInt(words[2]);
        }

        System.setOut(console);
        System.out.println(expression + result);
    }

    public static class TestString {
        public void printSomething() {
            System.out.print("3 + 6 = ");
        }
    }
}

