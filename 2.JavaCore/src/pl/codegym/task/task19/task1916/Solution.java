package pl.codegym.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/* 
Śledzenie zmian
*/

public class Solution {

    public static List<LineItem> lines = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
        reader.close();

        List<String> file1 = new ArrayList<>();
        List<String> file2 = new ArrayList<>();
        try (BufferedReader fileReader1 = new BufferedReader(new FileReader(filename1));
             BufferedReader fileReader2 = new BufferedReader(new FileReader(filename2)))
        {
            while (fileReader1.ready()) {
                file1.add(fileReader1.readLine());
            }
            while (fileReader2.ready()) {
                file2.add(fileReader2.readLine());
            }
        } catch (IOException exception) {
            System.out.println("File opening error!");
        }

        compareAndMergeLists(file1, file2);
    }

    private static void compareAndMergeLists(List<String> file1, List<String> file2) {
        int oldFileLine = 0;
        int newFileLine = 0;

        while ((oldFileLine < file1.size()) && (newFileLine < file2.size())) {

            if (file1.get(oldFileLine).equals(file2.get(newFileLine))) {
                lines.add(new LineItem(Type.SAME, file1.get(oldFileLine)));
                oldFileLine++;
                newFileLine++;
            } else if ((newFileLine + 1 < file2.size()) && file1.get(oldFileLine).equals(file2.get(newFileLine + 1))) {
                lines.add(new LineItem(Type.ADDED, file2.get(newFileLine)));
                newFileLine++;
            } else if ((oldFileLine + 1 < file1.size()) && file1.get(oldFileLine + 1).equals(file2.get(newFileLine))) {
                lines.add(new LineItem(Type.REMOVED, file1.get(oldFileLine)));
                oldFileLine++;
            }
        }

        while (oldFileLine < (file1.size())) {
            lines.add(new LineItem(Type.REMOVED, file1.get(oldFileLine)));
            oldFileLine++;
        }
        while (newFileLine < (file2.size())) {
            lines.add(new LineItem(Type.ADDED, file2.get(newFileLine)));
            newFileLine++;
        }
    }

    public static enum Type {
        ADDED,        // Dodana nowa linia
        REMOVED,      // Linia usunięta
        SAME          // Bez zmian
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}
