package pl.codegym.task.task19.task1922;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Wyszukiwanie właściwych linii
*/

public class Solution {
    public static List<String> words = new ArrayList<>();

    static {
        words.add("file");
        words.add("view");
        words.add("In");
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        reader.close();

        List<String> fileLines = readFile(filename);
        List<String> matchingLines = filterFileAgainstWords(fileLines);
        printList(matchingLines);
    }

    private static void printList(List<String> matchingLines) {
        for (String string : matchingLines) {
            System.out.println(string);
        }
    }

    private static List<String> filterFileAgainstWords(List<String> fileLines) {
        List<String> result = new ArrayList<>();
        long counter = 0;
        for (String string : fileLines) {
            String[] words = string.split(" ");
            for (String word : words) {
                counter += Solution.words.stream()
                        .filter(el -> el.equals(word))
                        .count();
            }
            if (counter == 2) {
                result.add(string);
            }
            counter = 0;
        }

        return result;
    }

    private static List<String> readFile(String filename) throws IOException {
        List<String> lines = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }

        reader.close();

        return lines;
    }
}
