package pl.codegym.task.task19.task1907;

/* 
Liczenie słów
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        System.out.println(countWord(filename, "świat"));


        reader.close();
    }

    private static int countWord(String filename, String word) throws IOException {
        FileReader fileReader = new FileReader(filename);

        word = " " + word;
        char[] chars = word.toCharArray();

        int index = 0;
        int counter = 0;
        while (fileReader.ready()) {
            int data = fileReader.read();
            if (data == (int) chars[index]) {
                ++index;
                if (index == chars.length) {
                    counter++;
                    index = 0;
                }
            } else {
                index = 0;
            }
        }
        fileReader.close();

        return counter;
    }
}
