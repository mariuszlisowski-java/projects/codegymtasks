package pl.codegym.task.task19.task1904;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/* 
Jeszcze jeden adapter
*/

public class Solution {

    public static void main(String[] args) throws IOException {
        PersonScanner personScanner = new PersonScannerAdapter(new Scanner(System.in));
        Person person = personScanner.read();
        personScanner.close();

        System.out.println(person);
    }

    public static class PersonScannerAdapter implements PersonScanner{
        private Scanner fileScanner;

        public PersonScannerAdapter(Scanner fileScanner) {
            this.fileScanner = fileScanner;
        }

        @Override
        public Person read() throws IOException {
            String line = fileScanner.nextLine();
            String[] lines = line.split(" ");

            DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
            Date date;
            try {
                date = dateFormat.parse(lines[3] + lines[4] + lines[5]);
            } catch (ParseException e) {
                System.out.println("Wrong date format!");
                return null;
            }

            return new Person(lines[2], lines[0], lines[1], date);
        }

        @Override
        public void close() throws IOException {
            fileScanner.close();
        }
    }
}
