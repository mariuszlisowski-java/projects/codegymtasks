package pl.codegym.task.task19.task1903;

/* 
Dostosowywanie wielu interfejsów
*/

import java.util.HashMap;
import java.util.Map;

public class Solution {

    public static Map<String, String> countries = new HashMap<>();

    static {
        countries.put("UA", "Ukraina");
        countries.put("US", "Stany Zjednoczone");
        countries.put("FR", "Francja");
    }

    public static void main(String[] args) {
        IncomeData incomeData = new IncomeData() {
            public String getCountryCode() { return "US"; }
            public String getCompany() { return "CodeGym Ltd."; }
            public String getContactFirstName() { return "John"; }
            public String getContactLastName() { return "Smith"; }
            public int getCountryPhoneCode() { return 1; }
            public int getPhoneNumber() { return 991234567; }
        };

        Customer customer = new IncomeDataAdapter(incomeData);
        Contact contact = new IncomeDataAdapter(incomeData);

        System.out.println(customer.getCompanyName());
        System.out.println(customer.getCountryName());

        System.out.println(contact.getName());
        System.out.println(contact.getPhoneNumber());
    }

    public static class IncomeDataAdapter implements Customer, Contact {
        private IncomeData data;

        public IncomeDataAdapter(IncomeData incomeData) {
            this.data = incomeData;
        }

        // customer
        public String getCompanyName() {
            return data.getCompany();
        }

        public String getCountryName() {
            return countries.get(data.getCountryCode());
        }

        // contact
        public String getName() {
            return data.getContactLastName() + ", " + data.getContactFirstName();
        }

        public String getPhoneNumber() {
            StringBuilder phoneNumber = new StringBuilder(Integer.toString(data.getPhoneNumber()));
            int countryCode = data.getCountryPhoneCode();

            while(phoneNumber.length() < 10){
                phoneNumber.insert(0, "0");
            }

            return  "+" + countryCode + "(" +
                    phoneNumber.substring(0,3) + ")" +
                    phoneNumber.substring(3,6) + "-" +
                    phoneNumber.substring(6,8) + "-" +
                    phoneNumber.substring(8,10);
        }
    }

    public interface IncomeData {
        String getCountryCode();        // Na przykład: US
        String getCompany();            // Na przykład: CodeGym Ltd.
        String getContactFirstName();   // Na przykład: John
        String getContactLastName();    // Na przykład: Smith
        int getCountryPhoneCode();      // Na przykład: 1
        int getPhoneNumber();           // Na przykład: 991234567
    }

    public interface Customer {
        String getCompanyName();        // Na przykład: CodeGym Ltd.
        String getCountryName();        // Na przykład: Stany Zjednoczone
    }

    public interface Contact {
        String getName();               // Na przykład: Smith, John
        String getPhoneNumber();        // Na przykład: +1(099)123-45-67
    }
}
