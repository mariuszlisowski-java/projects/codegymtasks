package pl.codegym.task.task19.task1925;

/* 
Long words
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        String file1;
        String file2;
        if (args.length == 2) {
            file1 = args[0];
            file2 = args[1];
        } else {
            return;
        }

        List<String> file1Lines = readFile(file1);
        String modified = modifyByCommaLongerThanSixChars(file1Lines);
        writeFile(file2, modified);
    }

    private static void writeFile(String filename, String content) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            writer.write(content);
        } catch (IOException exception) {
            System.out.println("Fire writing error!");
        }
    }

    private static String modifyByCommaLongerThanSixChars(List<String> strings) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strings) {
            String[] words = string.split(" ");
            for (String word : words) {
                if (word.length() > 6) {
                    stringBuilder.append(word).append(",");
                }
            }
        }
        if (stringBuilder.charAt(stringBuilder.length() - 1) == ',') {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }

        return stringBuilder.toString();
    }

    private static List<String> readFile(String filename) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException exception) {
            System.out.println("File opening error!");
        }

        return lines;
    }
}
