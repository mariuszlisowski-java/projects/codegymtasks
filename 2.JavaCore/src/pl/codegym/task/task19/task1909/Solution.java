package pl.codegym.task.task19.task1909;

/* 
Zmiana znaków interpunkcyjnych
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputFilename = reader.readLine();
        String outputFilename = reader.readLine();

        writeFileWithReplaceDots(inputFilename, outputFilename);

        reader.close();
    }

    private static void writeFileWithReplaceDots(String inFilename, String outFilename) {
        try (BufferedReader fileReader = new BufferedReader(new FileReader(inFilename));
             BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outFilename)))
        {
            String line;
            while ((line = fileReader.readLine()) != null) {
                String lineWitDotsReplaced = replaceDotsWithExclamationMarks(line);
                fileWriter.write(lineWitDotsReplaced + "\n");
            }
        } catch (IOException exception) {
            System.out.println("IOError " + exception.getMessage());
        }
    }

    private static String replaceDotsWithExclamationMarks(String line) {
        return line.replace(".", "!");
    }
}
