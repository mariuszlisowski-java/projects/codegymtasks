package pl.codegym.task.task19.task1920;

/* 
Najbogatszy
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Solution {
    public static void main(String[] args) throws IOException {
        String filename;
        if (args.length > 0) {
            filename = args[0];
        } else {
            return;
        }

        printMap(sortByKeyAscendingWithSameMaxValue(sortByValueDecending(extractFile(filename))));
    }

    private static Map<String, Double> sortByKeyAscendingWithSameMaxValue(LinkedHashMap<String, Double> map) {
        Map<String, Double> sorted = new TreeMap<>();
        Double max = ((map.values()).iterator()).next();
        for (Map.Entry<String, Double> pair : map.entrySet()) {
            if (max.equals(pair.getValue())) {
                sorted.put(pair.getKey(), pair.getValue());
            }
        }

        return sorted;
    }

    private static void printMap(Map<String, Double> map) {
        for (String string : map.keySet()) {
            System.out.println(string);
        }
    }

    private static LinkedHashMap<String, Double> sortByValueDecending(Map<String, Double> map) {
        return map.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e2,
                        LinkedHashMap::new));
    }

    private static Map<String, Double> extractFile(String filename) throws IOException {
        Map<String, Double> fileLines = new HashMap<>();
        BufferedReader fileReader = new BufferedReader(new FileReader(filename));
        String line;
        while ((line = fileReader.readLine()) != null) {
            String[] lines = line.split(" ");
            fileLines.put(lines[0], fileLines.getOrDefault(lines[0], 0.0) + Double.parseDouble(lines[1]));
        }
        fileReader.close();

        return fileLines;
    }


}
