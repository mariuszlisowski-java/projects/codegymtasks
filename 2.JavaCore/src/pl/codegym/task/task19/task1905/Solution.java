package pl.codegym.task.task19.task1905;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/* 
Wzmocnij adapter
*/

public class Solution {

    public static Map<String,String> countries = new HashMap<>();

    static {
        countries.put("UA", "Ukraina");
        countries.put("US", "Stany Zjednoczone");
        countries.put("FR", "Francja");
    }

    public static void main(String[] args) {
        Customer customer = new Customer() {
            public String getCompanyName() { return "CodeGym Ltd."; }
            public String getCountryName() { return "Stany Zjednoczone"; }};
        Contact contact = new Contact() {
            public String getName() { return "Peterson, John"; }
            public String getPhoneNumber() { return "+1(111)222-3333"; }};

        RowItem rowItem = new DataAdapter(customer, contact);

        System.out.println(rowItem.getCountryCode());
        System.out.println(rowItem.getCompany());
        System.out.println(rowItem.getContactFirstName());
        System.out.println(rowItem.getContactLastName());
        System.out.println(rowItem.getDialString());
    }

    public static class DataAdapter implements RowItem {
        private Customer customer;
        private Contact contact;

        public DataAdapter(Customer customer, Contact contact) {
            this.customer = customer;
            this.contact = contact;
        }

        @Override
        public String getCountryCode() {
//            countries.put("US", "Stany Zjednoczone");
            String countryName = customer.getCountryName();
            return countries.entrySet()
                    .stream()
                    .filter(entry -> countryName.equals(entry.getValue()))
                    .map(Map.Entry::getKey)
                    .collect(Collectors.collectingAndThen(
                            Collectors.toList(),
                            list -> list.get(0)
                    ));
        }

        @Override
        public String getCompany() {
            return customer.getCompanyName();
        }

        @Override
        public String getContactFirstName() {
            String[] names = contact.getName().split(" ");
            return names[1];
        }

        @Override
        public String getContactLastName() {
            String[] names = contact.getName().split(" ");
//            return names[0].substring(0, names.length - 2);
            return names[0].substring(0, names[0].length() - 1);
        }

        @Override
        public String getDialString() {
            return "callto://" + contact
                    .getPhoneNumber()
                    .trim()
                    .replace("(", "")
                    .replace(")", "")
                    .replace("-", "");
        } // +1(111)222-3333
    }

    public interface RowItem {
        String getCountryCode();        // Na przykład: US
        String getCompany();            // Na przykład: CodeGym Ltd.
        String getContactFirstName();   // Na przykład: John
        String getContactLastName();    // Na przykład: Peterson
        String getDialString();         // Na przykład: callto://+11112223333
    }

    public interface Customer {
        String getCompanyName();        // Na przykład: CodeGym Ltd.
        String getCountryName();        // Na przykład: Stany Zjednoczone
    }

    public interface Contact {
        String getName();               // Na przykład: Peterson, John
        String getPhoneNumber();        // Na przykład: +1(111)222-3333, +3(805)0123-4567, +380(50)123-4567, etc.
    }
}