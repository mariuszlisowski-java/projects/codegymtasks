package pl.codegym.task.task19.task1919;

/* 
Obliczanie wynagrodzeń
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        String filename;
        if (args.length > 0) {
            filename = args[0];
        } else {
            return;
        }

        printMap(extractFile(filename));
    }

    private static void printMap(Map<String, Double> extractedFile) {
        for (Map.Entry<String, Double> map : extractedFile.entrySet()) {
            System.out.println(map.getKey() + " " + map.getValue());
        }
    }

    private static Map<String, Double> extractFile(String filename) throws IOException {
        Map<String, Double> fileLines = new TreeMap<>();
        BufferedReader fileReader = new BufferedReader(new FileReader(filename));
        String line;
        while ((line = fileReader.readLine()) != null) {
            String[] lines = line.split(" ");
            fileLines.put(lines[0], fileLines.getOrDefault(lines[0], 0.0) + Double.parseDouble(lines[1]));
        }
        fileReader.close();

        return fileLines;
    }
}
