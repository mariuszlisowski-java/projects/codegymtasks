package pl.codegym.task.task19.task1906;

/* 
Znaki parzyste
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputFilename = reader.readLine();
        String outputFilename = reader.readLine();

        readEvenChars(inputFilename, outputFilename);

        reader.close();
    }

    private static void readEvenChars(String inFilename, String outFilename) throws IOException {
        FileReader fileReader = new FileReader(inFilename);
        FileWriter fileWriter = new FileWriter(outFilename);

        int counter = 0;
        while (fileReader.ready()) {
            int data = fileReader.read();
            if (++counter % 2 == 0) {
                fileWriter.write(data);
            }
        }

        fileReader.close();
        fileWriter.close();
    }
}
