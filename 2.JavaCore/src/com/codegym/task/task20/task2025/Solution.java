package com.codegym.task.task20.task2025;

import java.util.Arrays;
import java.util.TreeSet;

/*
Number algorithms

*/

public class Solution {
    static TreeSet<Long> numbers;
    static long[][] pows;
    static int[] digits;

//    Result for N = 370  => [1, 2, 3, 4, 5, 6, 7, 8, 9, 153]
//    Result for N = 8208 =>  [1, 2, 3, 4, 5, 6, 7, 8, 9, 153, 370, 371, 407, 1634]
    public static void main(String[] args) {
        long a = System.currentTimeMillis();
        System.out.println(Arrays.toString(getNumbers(370)));
        long b = System.currentTimeMillis();
        System.out.println("memory " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (8 * 1024));
        System.out.println("time = " + (b - a) / 1000);

        a = System.currentTimeMillis();
        System.out.println(Arrays.toString(getNumbers(8208)));
        b = System.currentTimeMillis();
        System.out.println("memory " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (8 * 1024));
        System.out.println("time = " + (b - a) / 1000);
    }

    public static long[] getNumbers(long N) {
        long result[] = null;
        if (N <= 0) {
            return result;
        }

        int digits = 1 + (int)Math.log10(N);

        genpow(digits + 1);
        numbers = new TreeSet<>();
        for (int i = 1; i <= digits; i++) {
            try {
                search(i, 1,0, 0);
            } catch (Exception ignored) {}
        }
        numbers.removeIf(l -> l >= N);
        result = new long[numbers.size() - 1];
        int i = 0;
        for (long l : numbers) {
            if (l != 0) {
                result[i++] = l;
            }
        }

        return result;
    }

    public static void search(int N, int d, long pow, int index) {
        if (d == N + 1) {
            if (check(pow)) {
                numbers.add(pow);
            }
        } else {
            for (int i = index; i< 10; i++) {
                try {
                    search(N, d+1, pow+pows[i][N-1], i);
                } catch (Exception ignored) {}
            }
        }
    }

    public static boolean check(long number) {
        long temp = number;
        digits = new int[10];
        int n = 0;
        long sum = 0;
        while (temp > 0){
            int digit = (int)(temp % 10);
            digits[digit]++;
            n++;
            temp /= 10;
        }
        if (number == 0) n = 1;
        for (int i = 0; i < 10; i++) {
            sum += digits[i] * pows[i][n - 1];
        }

        return sum == number;
    }

    public static void genpow(int m) {
        pows = new long[10][m];
        for (int i = 0; i < 10; i++) {
            long p =1;
            for (int j = 0; j < m; j++) {
                p *= i;
                pows[i][j] = p;
            }
        }
    }

}
