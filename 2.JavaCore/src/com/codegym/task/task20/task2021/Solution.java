package com.codegym.task.task20.task2021;

import java.io.*;

/* 
Serialization is prohibited

*/

public class Solution implements Serializable {
    public static class SubSolution extends Solution {
        private void readObject(ObjectInputStream ois) throws NotSerializableException {
            throw new NotSerializableException();
        }
        private void writeObject(ObjectOutputStream ous) throws NotSerializableException {
            throw new NotSerializableException();
        }
    }

    public static void main(String[] args) {

    }
}
