package pl.codegym.task.task07.task0704;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Odwróć tablicę
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] ints = new int[10];
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < ints.length; i++) {
            ints[i] = scan.nextInt();
        }
        for (int i = 9; i >= 0 ; i--) {
            System.out.println(ints[i]);
        }
    }
}

