package pl.codegym.task.task07.task0708;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/* 
Najdłuższy ciąg
*/

public class Solution {
    private static List<String> strings;

    public static void main(String[] args) throws Exception {
        strings = new ArrayList<>();
        ArrayList<String> longests = new ArrayList<>();

        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

        int longest = 0;
        for (int i = 0; i < 5; i++) {
            strings.add(reader.readLine());
            if (strings.get(i).length() > longest) {
                longest = strings.get(i).length();
            }
        }
        for (String string : strings) {
            if (string.length() == longest) {
                longests.add(string);
            }
        }
        for (String string : longests) {
            System.out.println(string);
        }
    }

}
