package pl.codegym.task.task07.task0727;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Zmiana funkcjonalności
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<String> lista = new ArrayList<String>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            lista.add(s);
        }

        for (String el : lista) {
            if (el.length() % 2 == 0) {
                System.out.print(el + " ");
                System.out.println(el);
            } else {
                System.out.print(el + " ");
                System.out.print(el + " ");
                System.out.println(el);
            }
        }

    }
}
