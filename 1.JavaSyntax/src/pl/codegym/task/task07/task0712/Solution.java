package pl.codegym.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Najkrótszy czy najdłuższy
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();

        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

        int longest = Integer.MIN_VALUE;
        int shortest = Integer.MAX_VALUE;
        int longest_index = 0;
        int shortest_index = 0;
        for (int i = 0; i < 10; i++) {
            list.add(reader.readLine());
            if (list.get(i).length() < shortest) {
                shortest = list.get(i).length();
                shortest_index = i;
            }
            if (list.get(i).length() > longest) {
                longest = list.get(i).length();
                longest_index = i;
            }
        }
        if (shortest_index < longest_index) {
            System.out.println(list.get(shortest_index));
        } else {
            System.out.println(list.get(longest_index));
        }
    }
}
