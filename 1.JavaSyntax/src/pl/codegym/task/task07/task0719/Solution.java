package pl.codegym.task.task07.task0719;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

/* 
Wyświetl liczby w odwrotnej kolejności
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> ints = new ArrayList<>(10);

//        Collections.addAll(ints, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

        for (int i = 0; i < 10; i++) {
            ints.add(Integer.parseInt(reader.readLine()));
        }
        Collections.reverse(ints);
        for (Integer el : ints) {
            System.out.println(el);
        }
    }

}
