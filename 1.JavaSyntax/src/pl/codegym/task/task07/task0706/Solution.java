package pl.codegym.task.task07.task0706;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Ulice i domy
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] ints = new int[15];
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < ints.length; i++) {
            ints[i] = scanner.nextInt();
        }
        int even = 0, odd = 0;
        for (int i = 0; i < ints.length; i++) {
            if (i % 2 == 0) {
                even += ints[i];
            } else {
                odd += ints[i];
            }
        }
        System.out.println(
                even > odd ?
                "Więcej ludzi mieszka w domach o parzystych numerach." :
                "Więcej ludzi mieszka w domach o nieparzystych numerach."
        );
    }

}
