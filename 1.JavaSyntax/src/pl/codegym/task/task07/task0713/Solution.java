package pl.codegym.task.task07.task0713;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* 
Granie w Javarellę
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<Integer> mainList = new ArrayList<>();
//        List<Integer> mainList = new ArrayList<>();
        ArrayList<Integer> byThree = new ArrayList<>();
        ArrayList<Integer> byTwo = new ArrayList<>();
        ArrayList<Integer> theRest = new ArrayList<>();

        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

//        mainList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        for (int i = 0; i < 20; i++) {
            mainList.add(Integer.parseInt(reader.readLine()));
        }
        for (Integer el : mainList) {
            if (el % 2 != 0 &&
                el % 3 != 0) {
                theRest.add(el);
            } else {
                if (el % 3 == 0) {
                    byThree.add(el);
                }
                if (el % 2 == 0) {
                    byTwo.add(el);
                }
            }
        }
//        System.out.println("------------------");
        printLista(byThree);
//        System.out.println("------------------");
        printLista(byTwo);
//        System.out.println("------------------");
        printLista(theRest);
    }

    public static void printLista(List<Integer> lista) {
        for (Integer el : lista) {
            System.out.println(el);
        }
    }
}
