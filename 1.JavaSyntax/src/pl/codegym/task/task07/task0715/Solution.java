package pl.codegym.task.task07.task0715;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/* 
Więcej Ala ma kota
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, "Ala", "ma", "kota");

        int size = list.size();
        for (int i = 1; i <= size + 2; i += 2) {
            if (i == size + 1) {
                list.add("As");
            } else {
                list.add(i, "As");
            }
        }
        for (String el : list) {
            System.out.println(el);
        }
    }
}
