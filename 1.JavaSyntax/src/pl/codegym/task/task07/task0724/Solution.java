package pl.codegym.task.task07.task0724;

/* 
Spis członków rodziny
*/

public class Solution {
    public static void main(String[] args) {
        Ludzie dziadek1 = new Ludzie("Jan", true, 77);
        Ludzie dziadek2 = new Ludzie("Stanislaw", true, 65);
        Ludzie babcia1 = new Ludzie("Janina", false, 66);
        Ludzie babcia2 = new Ludzie("Stanislawa", false, 79);
        Ludzie ojciec = new Ludzie("Marian", true, 52, dziadek1, babcia1);
        Ludzie matka = new Ludzie("Marianna", false, 47, dziadek2, babcia2);
        Ludzie dziecko1 = new Ludzie("Kamil", true, 18, ojciec, matka);
        Ludzie dziecko2 = new Ludzie("Agata", false, 15, ojciec, matka);
        Ludzie dziecko3 = new Ludzie("Roman", true, 12, ojciec, matka);

        System.out.println(dziadek1);
        System.out.println(dziadek2);
        System.out.println(babcia1);
        System.out.println(babcia2);
        System.out.println(ojciec);
        System.out.println(matka);
        System.out.println(dziecko1);
        System.out.println(dziecko2);
        System.out.println(dziecko3);
    }

    public static class Ludzie {
        String imie;
        boolean plec;
        int wiek;
        Ludzie ojciec;
        Ludzie matka;

        public Ludzie(String imie, boolean plec, int wiek) {
            this.imie = imie;
            this.plec = plec;
            this.wiek = wiek;
        }
        public Ludzie(String imie, boolean plec, int wiek, Ludzie ojciec, Ludzie matka) {
            this.imie = imie;
            this.plec = plec;
            this.wiek = wiek;
            this.ojciec = ojciec;
            this.matka = matka;
        }

        public String toString() {
            String tekst = "";
            tekst += "Imię: " + this.imie;
            tekst += ", płeć: " + (this.plec ? "mężczyzna" : "kobieta");
            tekst += ", wiek: " + this.wiek;

            if (this.ojciec != null)
                tekst += ", ojciec: " + this.ojciec.imie;

            if (this.matka != null)
                tekst += ", matka: " + this.matka.imie;

            return tekst;
        }
    }
}