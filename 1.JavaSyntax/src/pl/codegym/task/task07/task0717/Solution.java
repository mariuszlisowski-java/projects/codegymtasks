package pl.codegym.task.task07.task0717;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/* 
Duplikowanie słów
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> lista = new ArrayList<>();
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

//        Collections.addAll(lista, "alfa", "beta", "gamma", "theta", "delta");
        for (int i = 0; i < 10; i++) {
            lista.add(reader.readLine());
        }

        ArrayList<String> wynik = doubleValues(lista);

        for (String el : lista) {
            System.out.println(el);
        }
    }

    public static ArrayList<String> doubleValues(ArrayList<String> lista) {
        for (int i = 0; i < lista.size(); i += 2) {
                lista.add(i, lista.get(i));
        }

        return lista;
    }
}
