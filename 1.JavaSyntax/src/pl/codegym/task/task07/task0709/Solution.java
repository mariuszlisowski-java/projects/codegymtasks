package pl.codegym.task.task07.task0709;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Mów zwięźle
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> lista = new ArrayList<>();
        ArrayList<String> shortests = new ArrayList<>();

        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

        int shortest = Integer.MAX_VALUE;
        for (int i = 0; i < 5; i++) {
            lista.add(reader.readLine());
            if (lista.get(i).length() < shortest) {
                shortest = lista.get(i).length();
            }
        }
        for (String string : lista) {
            if (string.length() == shortest) {
                shortests.add(string);
            }
        }
        for (String string : shortests) {
            System.out.println(string);
        }





    }
}
