package pl.codegym.task.task07.task0721;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/* 
Minimum i maksimum w tablicach
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int maksimum = Integer.MIN_VALUE;
        int minimum = Integer.MAX_VALUE;
        int ARRAY_SIZE = 20;

        int[] numbers = new int[20];
//        int[] numbers = {66, 1, 2, -33, 4, 5, 0, 66, -33, 5};

        for (int i = 0; i < ARRAY_SIZE; i++) {
            numbers[i] = Integer.parseInt(reader.readLine());
        }
        for (int el : numbers) {
            if (el > maksimum) {
                maksimum = el;
            }
            if (el < minimum) {
                minimum = el;
            }
        }

        System.out.print(maksimum + " " + minimum);
    }
}
