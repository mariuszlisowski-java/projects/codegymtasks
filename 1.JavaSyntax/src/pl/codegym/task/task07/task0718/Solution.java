package pl.codegym.task.task07.task0718;

import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/* 
Sprawdzanie kolejności
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(r);

//        Collections.addAll(list, "a", "ab", "abc", "abcd", "a");
        for (int i = 0; i < 10; i++) {
            list.add(reader.readLine());
        }

        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).length() < list.get(i - 1).length()) {
                System.out.println(i);
            }
        }
    }
}

