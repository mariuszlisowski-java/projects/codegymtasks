package pl.codegym.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Jedna wielka tablica i dwie małe
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        int[] ints = new int[20];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = scan.nextInt();
        }
        int[] half1 = new int[10];
        int[] half2 = new int[10];
        for (int i = 0; i < ints.length; i++) {
            if (i <= 9) {
                half1[i] = ints[i];
            } else {
                half2[i - 10] = ints[i];
                System.out.println(half2[i - 10]);
            }
        }

    }
}
