package pl.codegym.task.task10.task1019;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Funkcjonalność to nie wszystko!
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        HashMap<String, Integer> map = new HashMap<>();
        while (true) {
            try {
                Integer id = Integer.parseInt(reader.readLine());
                String imie = reader.readLine();
                map.put(imie, id);
                if (imie.isEmpty() || imie.equals(" ") || imie == null) {
                    break;
                }
            } catch (Exception exception) {
                break;
            }
        }

        for (Map.Entry<String, Integer> pair : map.entrySet()) {
//            System.out.println("Id=" + pair.getValue() + " Imię=" + pair.getKey());
            System.out.println(pair.getValue() + " " + pair.getKey());
        }
    }
}
