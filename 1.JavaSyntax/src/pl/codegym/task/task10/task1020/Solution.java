package pl.codegym.task.task10.task1020;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/* 
Zadanie z algorytmami
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] tablica = new int[30];
        for (int i = 0; i < 30; i++) {
            tablica[i] = Integer.parseInt(reader.readLine());
        }

        sortuj(tablica);
        System.out.println(tablica[9]);
        System.out.println(tablica[10]);
    }

    public static void sortuj(int[] tablica) {
        List<Integer> collected = Arrays.stream(tablica)
                .boxed()
                .sorted()
                .collect(Collectors.toList());
        int ordinal = 0;
        for (Integer integer : collected) {
            tablica[ordinal++] = integer;
        }
    }
}
