package pl.codegym.task.task10.task1012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.Collator;
import java.util.*;

/* 
Liczba liter
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // Alfabet
        String abc = "aąbcćdeęfghijklłmnńoóprsśtuwyzźż";
        char[] abcTablica = abc.toCharArray();

        ArrayList<Character> alfabet = new ArrayList<>();
        for (char litera : abcTablica) {
            alfabet.add(litera);
        }

        // Czyta ciągi
        ArrayList<String> lista = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String s = reader.readLine();
            lista.add(s.toLowerCase());
        }
//        ArrayList<String> lista = new ArrayList<String>() {{
//            add("żż");
//            add("c");
//            add("ąą");
//            add("bbb");
//        }};

        Comparator<Character> polishComparator = new Comparator<Character>() {
            private Collator collator = Collator.getInstance(new Locale("pl", "PL"));
            @Override
            public int compare(Character ch1, Character ch2) {
                return collator.compare(ch1.toString(), ch2.toString());
            }
        };

        Map<Character, Long> lettersNumber = new TreeMap<>(polishComparator);
        for (String str : lista) {
            alfabet.forEach(character -> {
                        long count = str.chars()
                                        .filter(ch -> ch == character)
                                        .count();
                            lettersNumber.merge(character, count, Long::sum);
                    });
        }

        for (Map.Entry<Character, Long> pair : lettersNumber.entrySet()) {
            System.out.println(pair.getKey() + " " + pair.getValue());
        }
    }

}
