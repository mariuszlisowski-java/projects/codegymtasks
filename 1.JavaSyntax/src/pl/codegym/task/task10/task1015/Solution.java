package pl.codegym.task.task10.task1015;

import java.util.ArrayList;

/* 
Tablica z listami ciągów
*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<String>[] tablicaCiagowList = utworzList();
        printList(tablicaCiagowList);
    }

    public static ArrayList<String>[] utworzList() {
        ArrayList<String>[] tablica = new ArrayList[2];
        tablica[0] = new ArrayList<String>() {{
            add("ala");
            add("ma");
            add("kota");
        }};
        tablica[1] = new ArrayList<String>() {{
            add("a");
            add("kot");
            add("ale");
        }};

        return tablica;
    }

    public static void printList(ArrayList<String>[] tablicaCiagowList) {
        for (ArrayList<String> lista : tablicaCiagowList) {
            for (String s : lista) {
                System.out.println(s);
            }
        }
    }
}