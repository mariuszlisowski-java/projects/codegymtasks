package pl.codegym.task.task10.task1013;

/* 
Konstruktory klasy Ludzie
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Ludzie {
        private String nationality;
        private String race;
        private Integer age;
        private Boolean sex;
        private Float height;
        private Float weight;

        public Ludzie(String race) {
            this.race = race;
        }

        public Ludzie(Integer age) {
            this.age = age;
        }

        public Ludzie(Boolean sex) {
            this.sex = sex;
        }

        public Ludzie(Float height) {
            this.height = height;
        }

        public Ludzie(String nationality, String race) {
            this.nationality = nationality;
            this.race = race;
        }

        public Ludzie(String nationality, Integer age) {
            this.nationality = nationality;
            this.age = age;
        }

        public Ludzie(String nationality, String race, Integer age) {
            this.nationality = nationality;
            this.race = race;
            this.age = age;
        }

        public Ludzie(Boolean sex, Float weight) {
            this.sex = sex;
            this.weight = weight;
        }

        public Ludzie(Boolean sex, Float height, Float weight) {
            this.sex = sex;
            this.height = height;
            this.weight = weight;
        }

        public Ludzie(String nationality, String race, Integer age, Boolean sex) {
            this.nationality = nationality;
            this.race = race;
            this.age = age;
            this.sex = sex;
        }
    }
}
