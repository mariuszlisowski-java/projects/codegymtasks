package pl.codegym.task.task03.task0303;

/* 
Wymiana walut
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(konwertujEurNaUsd(10, 1.2));
        System.out.println(konwertujEurNaUsd(20, 1.4));
    }

    public static double konwertujEurNaUsd(int eur, double kursWymiany) {
        return eur * kursWymiany;
    }
}
