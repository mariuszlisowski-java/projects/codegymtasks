package pl.codegym.task.task03.task0307;

/* 
Witaj, StarCraft!
*/

public class Solution {
    public static void main(String[] args) {
        Zerg zerg1 = new Zerg();
        Zerg zerg2 = new Zerg();
        Zerg zerg3 = new Zerg();
        Zerg zerg4 = new Zerg();
        Zerg zerg5 = new Zerg();

        zerg1.imie = "on1";
        zerg2.imie = "on2";
        zerg3.imie = "on3";
        zerg4.imie = "on4";
        zerg5.imie = "on5";

        Protoss protoss1 = new Protoss();
        Protoss protoss2 = new Protoss();
        Protoss protoss3 = new Protoss();

        protoss1.imie = "ono1";
        protoss2.imie = "ono2";
        protoss3.imie = "ono3";

        Terranin terranin1 = new Terranin();
        Terranin terranin2 = new Terranin();
        Terranin terranin3 = new Terranin();
        Terranin terranin4 = new Terranin();

        terranin1.imie = "oni1";
        terranin2.imie = "oni2";
        terranin3.imie = "oni3";
        terranin4.imie = "oni4";
    }

    public static class Zerg {
        public String imie;
    }

    public static class Protoss {
        public String imie;
    }

    public static class Terranin {
        public String imie;
    }
}
