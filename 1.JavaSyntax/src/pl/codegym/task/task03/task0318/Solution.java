package pl.codegym.task.task03.task0318;

/* 
Plan podboju świata
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String years = bufferedReader.readLine();
        String name = bufferedReader.readLine();
        System.out.println(name + " przejmie władzę nad światem za " + years + " lat. Mua ha ha!");
    }
}
