package pl.codegym.task.task03.task0314;

/* 
Tabliczka mnożenia
*/

public class Solution {
    public static void main(String[] args) {
        for (int j = 1; j <= 10; j++) {
            for (int i = 1; i <= 10; i++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
    }
}
