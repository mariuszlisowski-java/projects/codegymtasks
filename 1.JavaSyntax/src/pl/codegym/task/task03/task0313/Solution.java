package pl.codegym.task.task03.task0313;

/* 
Ala ma kota
*/

public class Solution {
    public static void main(String[] args) {
        String word1 = "Ala",
               word2 = "ma",
               word3 = "kota";
        System.out.println(word1 + word2 + word3);
        System.out.println(word1 + word3 + word2);
        System.out.println(word2 + word1 + word3);
        System.out.println(word2 + word3 + word1);
        System.out.println(word3 + word2 + word1);
        System.out.println(word3 + word1 + word2);
    }
}
