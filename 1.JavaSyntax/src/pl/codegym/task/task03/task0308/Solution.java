package pl.codegym.task.task03.task0308;

/* 
Iloczyn 10 liczb
*/

public class Solution {
    public static void main(String[] args) {
        int sum = 1;
        for (int i = 1; i <= 10; ++i) {
            sum *= i;
        }
        System.out.println(sum);
    }
}
