package pl.codegym.task.task06.task0622;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Liczby w kolejności rosnącej
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(reader.readLine());
        }
        bubbleSort(array);
        for (int j : array) {
            System.out.println(j);
        }
    }

    public static void bubbleSort(int[] array) {
        int numberOfSwaps = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    array[j] = array[j] ^ array[j + 1] ^ (array[j + 1] = array[j]);
                    numberOfSwaps++;
                }
                if (numberOfSwaps == 0) {
                    break;
                }
            }
        }
    }

}
