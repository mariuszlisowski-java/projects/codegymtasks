package pl.codegym.task.task06.task0621;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Relacje między kotami
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String imieDziadka = reader.readLine();
        String imieBabci = reader.readLine();
        String imieOjca = reader.readLine();
        String imieMatki = reader.readLine();
        String imieSyna = reader.readLine();
        String imieCorki = reader.readLine();

        Kot kotDziadek = new Kot(imieDziadka);
        Kot kotBabcia = new Kot(imieBabci);
        Kot kotOjciec = new Kot(imieOjca, kotDziadek);
        Kot kotMatka = new Kot(kotBabcia, imieMatki);
        Kot kotSyn = new Kot(imieSyna, kotMatka, kotOjciec);
        Kot kotCorka = new Kot(imieCorki, kotMatka, kotOjciec);

        System.out.println(kotDziadek);
        System.out.println(kotBabcia);
        System.out.println(kotOjciec);
        System.out.println(kotMatka);
        System.out.println(kotSyn);
        System.out.println(kotCorka);
    }

    public static class Kot {
        private String imie;
        private Kot matka;
        private Kot ojciec;

        Kot(String imie) {
            this.imie = imie;
        }
        Kot(Kot matka, String imie) {
            this.imie = imie;
            this.matka = matka;
        }
        Kot(String imie, Kot ojciec) {
            this.imie = imie;
            this.ojciec = ojciec;
        }
        Kot(String imie, Kot matka, Kot ojciec) {
            this.imie = imie;
            this.matka = matka;
            this.ojciec = ojciec;
        }

        @Override
        public String toString() {
            if (matka == null && ojciec == null) {
                return "Imię kota to " + imie + ", brak matki, brak ojca ";
            } if (matka == null) {
                return "Imię kota to " + imie + ", brak matki, " + ojciec.imie + " to ojciec";
            } if (ojciec == null) {
                return "Imię kota to " + imie + ", " + matka.imie + " to matka, brak ojca";
            } else {
                return "Imię kota to " + imie + ", " + matka.imie + " to matka, " + ojciec.imie + " to ojciec";
            }
        }

    }
}
