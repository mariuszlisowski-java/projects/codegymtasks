package pl.codegym.task.task06.task0606;

import java.io.*;
import java.util.Scanner;

/* 
Cyfry parzyste i nieparzyste
*/

public class Solution {

    public static int parzyste;
    public static int nieparzyste;

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        parzyste = 0;
        nieparzyste = 0;
        while (number > 0) {
            int digit = number % 10;
            if (digit % 2 == 0) {
                ++parzyste;
            } else {
                ++nieparzyste;
            }
            number /= 10;
        }
        System.out.println("Parzyste: " + parzyste + " Nieparzyste: " + nieparzyste);
    }
}
