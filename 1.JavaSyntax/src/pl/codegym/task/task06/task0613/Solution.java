package pl.codegym.task.task06.task0613;

/* 
Kot i statyczne
*/

public class Solution {
    public static void main(String[] args) {
        int CATS_NUM = 10;
        Kot[] cats = new Kot[CATS_NUM];
        for (int i = 0; i < cats.length; i++) {
            cats[i] = new Kot();
        }
        System.out.println(Kot.licznikKotow);
    }

    public static class Kot {
        public static int licznikKotow;

        public Kot() {
            ++licznikKotow;
        }
    }
}
