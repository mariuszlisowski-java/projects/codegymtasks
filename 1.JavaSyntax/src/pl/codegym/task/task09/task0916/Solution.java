package pl.codegym.task.task09.task0916;

import java.io.IOException;
import java.rmi.RemoteException;

/* 
Obsługiwanie wyjątków typu checked
*/

public class Solution {
    public static void main(String[] args) {
        obslugaExceptions(new Solution());

    }

    public static void obslugaExceptions(Solution obj) {
        try {
            obj.method1();
            obj.method2();
            obj.method3();
        } catch (NoSuchFieldException exception) {
            System.out.println(exception);
        } catch (RemoteException exception) {
            System.out.println(exception);
        } catch (IOException exception) {
            System.out.println(exception);
        }
    }

    public void method1() throws IOException {
        throw new IOException();
    }

    public void method2() throws NoSuchFieldException {
        throw new NoSuchFieldException();
    }

    public void method3() throws RemoteException {
        throw new RemoteException();
    }
}
