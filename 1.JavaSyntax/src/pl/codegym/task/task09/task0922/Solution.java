package pl.codegym.task.task09.task0922;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

/* 
Jaka dzisiaj jest data?
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", new Locale("pl"));
        inputFormat.setLenient(false);
        Date date;

        try {
            date = inputFormat.parse(reader.readLine());
            System.out.println(outputFormat.format(date).toUpperCase());
        } catch (IOException | ParseException exception) {
            System.out.println("Nieprawidłowy format daty");
        }
    }
}
