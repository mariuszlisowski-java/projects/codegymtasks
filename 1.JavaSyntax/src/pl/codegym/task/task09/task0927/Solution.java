package pl.codegym.task.task09.task0927;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* 
Dziesięć kotów
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, Kot> mapa = utworzMap();
        Set<Kot> set = konwertujMapDoSet(mapa);
        printSetKotow(set);
    }

    public static Map<String, Kot> utworzMap() {
        Map<String, Kot> koty = new HashMap<String, Kot>();
        Kot kot1 = new Kot("a");
        Kot kot2 = new Kot("b");
        Kot kot3 = new Kot("c");
        Kot kot4 = new Kot("d");
        Kot kot5 = new Kot("e");
        Kot kot6 = new Kot("f");
        Kot kot7 = new Kot("g");
        Kot kot8 = new Kot("h");
        Kot kot9 = new Kot("i");
        Kot kot10 = new Kot("j");
        koty.put(kot1.imie, kot1);
        koty.put(kot2.imie, kot2);
        koty.put(kot3.imie, kot3);
        koty.put(kot4.imie, kot4);
        koty.put(kot5.imie, kot5);
        koty.put(kot6.imie, kot6);
        koty.put(kot7.imie, kot7);
        koty.put(kot8.imie, kot8);
        koty.put(kot9.imie, kot9);
        koty.put(kot10.imie, kot10);

        return koty;
    }

    public static Set<Kot> konwertujMapDoSet(Map<String, Kot> mapa) {
        Set<Kot> setKoty = new HashSet<>(mapa.values());

        return setKoty;
    }

    public static void printSetKotow(Set<Kot> set) {
        for (Kot kot : set) {
            System.out.println(kot);
        }
    }

    public static class Kot {
        private String imie;

        public Kot(String imie) {
            this.imie = imie;
        }

        public String toString() {
            return "Kot " + this.imie;
        }
    }


}
