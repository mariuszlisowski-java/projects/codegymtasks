package pl.codegym.task.task09.task0926;

import java.util.ArrayList;
import java.util.Arrays;

/* 
Lista tablic z liczbami
*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<int[]> lista = utworzLista();
        printLista(lista);
    }

    public static ArrayList<int[]> utworzLista() {
        int[] arr1 = {1,2,3,4,5};
        int[] arr2 = {1,2};
        int[] arr3 = {1,2,3,4};
        int[] arr4 = {1,2,3,4,5,6,7};
        int[] arr5 = {};
        ArrayList<int[]> lista = new ArrayList<>();
        lista.add(arr1);
        lista.add(arr2);
        lista.add(arr3);
        lista.add(arr4);
        lista.add(arr5);

        return lista;
    }

    public static void printLista(ArrayList<int[]> lista) {
        for (int[] tablica : lista) {
            for (int x : tablica) {
                System.out.println(x);
            }
        }
    }
}
