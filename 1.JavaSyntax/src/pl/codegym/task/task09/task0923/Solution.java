package pl.codegym.task.task09.task0923;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Samogłoski i spółgłoski
*/

public class Solution {
    public static char[] samogloski = new char[]{'a', 'e', 'i', 'o', 'u'};

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input;
        List<Character> vowels = new ArrayList<>();
        List<Character> consonants = new ArrayList<>();

        try {
            input = reader.readLine();
            for (char character : input.toCharArray()) {
                if (character != ' ') {
                    if (isSamogloska(character)) {
                        vowels.add(character);
                    } else {
                        consonants.add(character);
                    }
                }
            }
        } catch (IOException exception) {

        }

        for (Character character : vowels) {
            System.out.print(character + " ");
        }
        System.out.println();
        for (Character character : consonants) {
            System.out.print(character + " ");
        }
    }

    // Ta metoda sprawdza, czy litera jest samogłoską
    public static boolean isSamogloska(char c) {
        c = Character.toLowerCase(c);  // Konwertuje na małe litery

        for (char d : samogloski)   // Szuka samogłosek w tablicy
        {
            if (c == d)
                return true;
        }
        return false;
    }
}