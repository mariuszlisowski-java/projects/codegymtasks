package pl.codegym.task.task05.task0503;

/* 
Gettery i settery dla klasy Pies
*/

public class Pies {
    private String imie;
    private int wiek;

    public String getImie() {
        return imie;
    }
    public void setImie(String name) {
        imie = name;
    }

    public int getWiek() {
        return wiek;
    }
    public void setWiek(int age) {
        wiek = age;
    }

    public static void main(String[] args) {

    }
}
