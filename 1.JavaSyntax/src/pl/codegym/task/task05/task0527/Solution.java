package pl.codegym.task.task05.task0527;

/* 
Tom i Jerry
*/

public class Solution {
    public static void main(String[] args) {
        Mysz jerryMysz = new Mysz("Jerry", 12, 5);
        Pies jerryPies = new Pies("Hacker", "grey", 3);
        Kot jerryKot = new Kot("Blinky", "white", 1);
    }

    public static class Mysz {

        String imie;
        int wzrost;
        int ogon;

        public Mysz(String imie, int wzrost, int ogon) {
            this.imie = imie;
            this.wzrost = wzrost;
            this.ogon = ogon;
        }
    }

    public static class Pies {
        String name_,
               color_;
        int age_;

        public Pies(String name, String color, int age) {
            this.name_ = name;
            this.color_ = color;
            this.age_ = age;
        }
    }

    public static class Kot {
        String name_,
               color_;
        int age_;

        public Kot(String name, String color, int age) {
            this.name_ = name;
            this.color_ = color;
            this.age_ = age;
        }
    }
}
