package pl.codegym.task.task05.task0507;

/* 
Średnia arytmetyczna
*/

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int sum = 0,
            counter = 0;
        while (true) {
            int value = scanner.nextInt();
            if (value == -1) {
                break;
            }
            sum += value;
            ++counter;
        }
        System.out.println((double)sum / counter);
    }
}

