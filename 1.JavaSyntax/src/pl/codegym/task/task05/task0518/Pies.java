package pl.codegym.task.task05.task0518;

/* 
Rejestr psów
*/

public class Pies {
    public String imie,
                  kolor;
    public int wzrost;

    public Pies(String imie){
        this.imie = imie;
        this.kolor = "black";
        this.wzrost = 20;
    }
    public Pies(String imie, int wzrost){
        this.imie = imie;
        this.wzrost = wzrost;
        this.kolor = "black";
    }
    public Pies(String imie, int wzrost, String kolor){
        this.imie = imie;
        this.wzrost = wzrost;
        this.kolor = kolor;
    }

    public static void main(String[] args) {

    }
}
