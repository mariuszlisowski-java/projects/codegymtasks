package pl.codegym.task.task05.task0504;

/* 
Trzej Muszkoterowie
*/

public class Solution {
    public static void main(String[] args) {
        Kot kot1 = new Kot("Mru", 2, 3, 19);
        Kot kot2 = new Kot("Miau", 3, 4, 22);
        Kot kot3 = new Kot("Mao", 1,2,15);
    }

    public static class Kot {
        public String imie;
        protected int wiek;
        int waga;
        private int sila;

        public Kot(String imie, int wiek, int waga, int sila) {
            this.imie = imie;
            this.wiek = wiek;
            this.waga = waga;
            this.sila = sila;
        }
    }
}