package pl.codegym.task.task05.task0513;

/* 
Stwórzmy sobie prostokąt
*/

public class Prostokat {
    public int gora,
               lewy,
               szerokosc,
               wysokosc;

    public void inicjalizuj(int gora, int lewy, int szerokosc, int wysokosc) {
        this.gora = gora;
        this.lewy = lewy;
        this.szerokosc = szerokosc;
        this.wysokosc = wysokosc;
    }
    public void inicjalizuj(int gora, int lewy) {
        this.gora = gora;
        this.lewy = lewy;
        this.szerokosc = 0;
        this.wysokosc = 0;
    }
    public void inicjalizuj(int gora, int lewy, int szerokosc) {
        this.gora = gora;
        this.lewy = lewy;
        this.szerokosc = szerokosc;
        this.wysokosc = szerokosc;
    }
    public void inicjalizuj(Prostokat prostokat) {
        this.gora = prostokat.gora;
        this.lewy = prostokat.lewy;
        this.szerokosc = prostokat.szerokosc;
        this.wysokosc = prostokat.wysokosc;
    }

    public static void main(String[] args) {

    }
}
