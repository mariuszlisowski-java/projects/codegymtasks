package pl.codegym.task.task05.task0508;

/* 
Gettery i settery dla klasy Osoba
*/

public class Osoba {
    private String imie;
    private int wiek;
    private char plec;

    public void setImie(String imie) {
        this.imie = imie;
    }
    public void setWiek(int wiek) {
        this.wiek = wiek;
    }
    public void setPlec(char plec) {
        this.plec = plec;
    }

    public String getImie() {
        return imie;
    }
    public int getWiek() {
        return wiek;
    }
    public char getPlec() {
        return plec;
    }

    public static void main(String[] args) {

    }
}
