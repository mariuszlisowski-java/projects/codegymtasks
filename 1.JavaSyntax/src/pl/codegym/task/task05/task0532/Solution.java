package pl.codegym.task.task05.task0532;

import java.io.*;

/* 
Zadanie z algorytmami
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int count = Integer.parseInt(reader.readLine());
        if (count > 0) {
            int maksimum = Integer.MIN_VALUE;
            for (int i = 0; i < count; i++) {
                int number = Integer.parseInt(reader.readLine());
                if (number > maksimum) {
                    maksimum = number;
                }
            }
            System.out.println(maksimum);
        }
    }
}
