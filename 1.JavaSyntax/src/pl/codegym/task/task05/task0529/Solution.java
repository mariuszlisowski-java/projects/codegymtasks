package pl.codegym.task.task05.task0529;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Bank-świnka na bazie konsoli
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int sum = 0;
        while (true) {
            String number = bufferedReader.readLine();
            if (number.equals("sumuj")) {
                break;
            }
            sum += Integer.parseInt(number);
        }
        System.out.println(sum);
    }
}
