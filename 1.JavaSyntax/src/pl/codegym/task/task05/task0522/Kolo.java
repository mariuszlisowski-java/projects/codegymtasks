package pl.codegym.task.task05.task0522;

/* 
Maksimum konstruktorów
*/

public class Kolo {

    public double x;
    public double y;
    public double promien;

    public Kolo() {
        this.x = 1;
        this.y = 1;
        this.promien = 5;
    }
    public Kolo(double x, double y) {
        this.x = x;
        this.y = y;
        this.promien = 5;
    }
    public Kolo(double x, double y, int promien) {
        this.x = x;
        this.y = y;
        this.promien = promien;
    }
    public Kolo(int promien) {
        this.x = 1;
        this.y = 1;
        this.promien = promien;
    }

    public static void main(String[] args) {

    }
}