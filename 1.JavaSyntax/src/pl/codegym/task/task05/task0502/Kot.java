package pl.codegym.task.task05.task0502;

/* 
Zaimplementuj metodę walka
*/

public class Kot {

    public int wiek;
    public int waga;
    public int sila;

    public Kot() {
    }

    public boolean walka(Kot innyKot) {
        if (this.sila >= innyKot.sila ||
            this.waga >= innyKot.waga ||
            this.wiek > innyKot.wiek) {
            return true;
        } else {
            return false;
        }

    }

    public static void main(String[] args) {
        Kot kot1 = new Kot();
        Kot kot2 = new Kot();

        kot1.waga = 3;
        kot1.sila = 20;
        kot1.wiek = 2;

        kot2.waga = 4;
        kot2.sila = 22;
        kot2.wiek = 2;

        System.out.println(kot1.walka(kot2));
        System.out.println(kot2.walka(kot1));
    }
}
