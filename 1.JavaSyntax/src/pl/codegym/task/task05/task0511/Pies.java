package pl.codegym.task.task05.task0511;

/* 
Utwórz klasę Pies
*/

public class Pies {
    public String imie,
                  kolor;
    public int wzrost;

    public void inicjalizuj(String imie) {
        this.imie = imie;
        this.wzrost = 30;
        this.kolor = "brown";
    }
    public void inicjalizuj(String imie, int wzrost) {
        this.imie = imie;
        this.wzrost = wzrost;
        this.kolor = "black";
    }
    public void inicjalizuj(String imie, int wzrost, String kolor) {
        this.imie = imie;
        this.wzrost = wzrost;
        this.kolor = kolor;
    }

    public static void main(String[] args) {

    }
}
