package pl.codegym.task.task05.task0509;

/* 
Utwórz klasę Przyjaciel
*/

public class Przyjaciel {
    public String imie;
    public int wiek;
    public char plec;

    public void inicjalizuj(String name) {
        imie = name;
    }
    public void inicjalizuj(String name, int age) {
        imie = name;
        wiek = age;
    }
    public void inicjalizuj(String name, int age, char sex) {
        imie = name;
        wiek = age;
        plec = sex;
    }

    public static void main(String[] args) {

    }
}
