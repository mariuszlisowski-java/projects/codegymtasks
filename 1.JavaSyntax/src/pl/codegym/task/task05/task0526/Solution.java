package pl.codegym.task.task05.task0526;

/* 
Mężczyzna i kobieta
*/

public class Solution {
    public static void main(String[] args) {
        Mezczyzna mezczyzna1 = new Mezczyzna("Jan", 47, "Kielce");
        Mezczyzna mezczyzna2 = new Mezczyzna("Mariusz", 43, "Krakow");
        System.out.println(mezczyzna1);
        System.out.println(mezczyzna2);

        Kobieta kobieta1 = new Kobieta("Ewelina", 40, "Kielce");
        Kobieta kobieta2 = new Kobieta("Aneta", 34, "Krakow");
        System.out.println((kobieta1));
        System.out.println((kobieta2));
    }

    public static class Mezczyzna {
        String imie,
               adres;
        int wiek;

        public Mezczyzna(String imie, int wiek, String adres) {
            this.imie = imie;
            this.adres = adres;
            this.wiek = wiek;
        }

        public String toString() {
            return imie + " " + wiek + " " + adres;
        }
    }

    public static class Kobieta {
        String imie,
               adres;
        int wiek;

        public Kobieta(String imie, int wiek, String adres) {
            this.imie = imie;
            this.adres = adres;
            this.wiek = wiek;
        }

        public String toString() {
            return imie + " " + wiek + " " + adres;
        }
    }
}
