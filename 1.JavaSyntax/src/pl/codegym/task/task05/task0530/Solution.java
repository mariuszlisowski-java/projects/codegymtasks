package pl.codegym.task.task05.task0530;

import java.io.*;

/* 
Szefie, mam tu coś dziwnego...
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String a = reader.readLine();
        String b = reader.readLine();

        int suma = Integer.parseInt(a) + Integer.parseInt(b);
        System.out.println("Suma = " + suma);
    }
}
