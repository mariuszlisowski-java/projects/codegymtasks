package pl.codegym.task.task05.task0517;

/* 
Tworzenie kotów
*/

public class Kot {
    public String imie,
                  adres,
                  kolor;
    public int wiek,
               waga;

    public Kot(String imie) {
        this.imie = imie;
        this.adres = null;
        this.kolor = "black";
        this.wiek = 1;
        this.waga = 1;
    }
    public Kot(String imie, int waga, int wiek) {
        this.imie = imie;
        this.adres = null;
        this.kolor = "black";
        this.wiek = wiek;
        this.waga = waga;
    }
    public Kot(String imie, int wiek) {
        this.imie = imie;
        this.adres = null;
        this.kolor = "black";
        this.wiek = wiek;
        this.waga = 1;
    }
    public Kot(int waga, String kolor) {
        this.imie = null;
        this.adres = null;
        this.kolor = kolor;
        this.wiek = 1;
        this.waga = waga;
    }
    public Kot(int waga, String kolor, String adres) {
        this.imie = null;
        this.adres = adres;
        this.kolor = kolor;
        this.wiek = 1;
        this.waga = waga;
    }

    public static void main(String[] args) {

    }
}
