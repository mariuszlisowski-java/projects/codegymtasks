package pl.codegym.task.task08.task0812;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/* 
Najdłuższa sekwencja
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(input);
        ArrayList<Integer> arrayList = new ArrayList<Integer>();

//        ArrayList<Integer> arrayList = new ArrayList<Integer>(
//                Arrays.asList(2, 4, 4, 4, 8, 8, 4, 12, 12, 14));

        for (int i = 0; i < 10; i++) {
            arrayList.add(Integer.parseInt(bufferedReader.readLine()));
        }

        int counter = 1, max = 1;
        for (int i = 0; i < arrayList.size() - 1; i++) {
            if (arrayList.get(i).equals(arrayList.get(i + 1))) {
                counter++;
                if (counter > max) {
                    max = counter;
                }
            } else {
                counter = 1;
            }
        }
        System.out.println(max);

    }

}