package pl.codegym.task.task08.task0824;

/* 
Załóż rodzinę
*/

import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        ArrayList<Ludzie> dziadowie = new ArrayList<>();
        ArrayList<Ludzie> dzieciDziadow1 = new ArrayList<>();
        ArrayList<Ludzie> dzieciDziadow2 = new ArrayList<>();
        ArrayList<Ludzie> dzieciRodzicow = new ArrayList<>();

        Ludzie dziadek1 = new Ludzie();
        dziadek1.imie = "Ludwig";
        dziadek1.plec = true;
        dziadek1.wiek = 77;
        Ludzie babcia1 = new Ludzie();
        babcia1.imie = "Michalina";
        babcia1.plec = false;
        babcia1.wiek = 92;
        Ludzie dziadek2 = new Ludzie();
        dziadek2.imie = "Stefan";
        dziadek2.plec = true;
        dziadek2.wiek = 72;
        Ludzie babcia2 = new Ludzie();
        babcia2.imie = "Stefania";
        babcia2.plec = false;
        babcia2.wiek = 69;
        Ludzie matka = new Ludzie();
        matka.imie = "Wieslawa";
        matka.plec = false;
        matka.wiek = 63;
        Ludzie ojciec = new Ludzie();
        ojciec.imie = "Stanislaw";
        ojciec.plec = true;
        ojciec.wiek = 64;
        Ludzie syn1 = new Ludzie();
        syn1.imie = "Mariusz";
        syn1.plec = true;
        syn1.wiek = 43;
        Ludzie syn2 = new Ludzie();
        syn2.imie = "Zbigniew";
        syn2.plec = true;
        syn2.wiek = 40;
        Ludzie corka = new Ludzie();
        corka.imie = "Emilka";
        corka.plec = false;
        corka.wiek = 22;

        dziadowie.add(dziadek1);
        dziadowie.add(dziadek2);
        dziadowie.add(babcia1);
        dziadowie.add(babcia2);
        dzieciDziadow1.add(ojciec);
        dzieciDziadow2.add(matka);
        dzieciRodzicow.add(syn1);
        dzieciRodzicow.add(syn2);
        dzieciRodzicow.add(corka);

        dziadek1.dzieci = dzieciDziadow1;
        babcia1.dzieci = dzieciDziadow1;
        dziadek2.dzieci = dzieciDziadow2;
        babcia2.dzieci = dzieciDziadow2;

        ojciec.dzieci = dzieciRodzicow;
        matka.dzieci = dzieciRodzicow;

//        syn1.dzieci = null;
//        syn2.dzieci = null;
//        corka.dzieci = null;

        System.out.println(dziadek1);
        System.out.println(babcia1);
        System.out.println(dziadek2);
        System.out.println(babcia2);

        System.out.println(ojciec);
        System.out.println(matka);

        System.out.println(syn1);
        System.out.println(syn2);
        System.out.println(corka);
    }

    public static class Ludzie {
        String imie;
        boolean plec;
        int wiek;
        ArrayList<Ludzie> dzieci;

        public String toString() {
            String tekst = "";
            tekst += "Imię: " + this.imie;
            tekst += ", płeć: " + (this.plec ? "mężczyzna" : "kobieta");
            tekst += ", wiek: " + this.wiek;

            if (dzieci != null) {
                int licznikDzieci = this.dzieci.size();
                if (licznikDzieci > 0) {
                    tekst += ", dzieci: " + this.dzieci.get(0).imie;

                    for (int i = 1; i < licznikDzieci; i++) {
                        Ludzie dziecko = this.dzieci.get(i);
                        tekst += ", " + dziecko.imie;
                    }
                }
            }

            return tekst;
        }
    }

}
