package pl.codegym.task.task08.task0827;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/* 
Praca z datami
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(isDataNieparzysta("MAY 1 2013"));
    }

    public static boolean isDataNieparzysta(String date) {
        Date passedDate = new Date(date);

        LocalDate localDate = passedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int currentYear = localDate.getYear();
        LocalDate firstDayOfCurrentYear = LocalDate.of(currentYear, Month.JANUARY, 1);

        long daysPassed = ChronoUnit.DAYS.between(firstDayOfCurrentYear, localDate);

        return daysPassed % 2 == 0;
    }

}