package pl.codegym.task.task08.task0822;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Najmniejsza z N liczb
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        List<Integer> listaLiczbCalkowitych = getListaLiczbCalkowitych();
        System.out.println(getMinimum(listaLiczbCalkowitych));
}

    public static int getMinimum(List<Integer> array) {
        if (array != null) {
            return Collections.min(array);
        } else {
            throw new NullPointerException();
        }

    }

    public static List<Integer> getListaLiczbCalkowitych() throws IOException {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();

        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            list.add(scanner.nextInt());
        }

        return list;
    }
}
