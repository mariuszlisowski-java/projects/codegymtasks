package pl.codegym.task.task08.task0823;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Ruszamy na cały kraj
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();

        StringBuilder output = new StringBuilder();
        boolean isLetter = true;

        for (char ch : s.toCharArray()) {
            if (ch != ' ' && isLetter) {
                output.append(Character.toUpperCase(ch));
            } else {
                output.append(ch);
            }
            isLetter = (ch == ' ');
        }

        System.out.println(output);
    }

}
