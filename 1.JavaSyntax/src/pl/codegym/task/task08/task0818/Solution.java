package pl.codegym.task.task08.task0818;

import sun.rmi.server.InactiveGroupException;

import java.util.HashMap;
import java.util.Map;

/* 
Tylko dla bogaczy
*/

public class Solution {
    public static HashMap<String, Integer> utworzMap() {
        HashMap<String, Integer> names = new HashMap<>();

        names.put("John", 600);
        names.put("Tom", 700);
        names.put("Joan", 900);
        names.put("Greg", 800);
        names.put("Olga", 500);
        names.put("Igor", 400);
        names.put("Mira", 300);
        names.put("Mike", 200);
        names.put("Gary", 100);
        names.put("Alice", 99);

        return names;

    }

    public static void usunPozycjeZmap(HashMap<String, Integer> map) {
        map.entrySet().removeIf(entry -> entry.getValue() < 500);
    }

    public static void main(String[] args) {

    }
}