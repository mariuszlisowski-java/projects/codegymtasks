package pl.codegym.task.task08.task0817;

import java.util.*;

/* 
Powtórzenia nie są potrzebne
*/

public class Solution {
    public static HashMap<String, String> utworzMap() {
        HashMap<String, String> names = new HashMap<>();

        names.put("A", "John");
        names.put("B", "Tom");
        names.put("C", "John");
        names.put("D", "John");
        names.put("E", "John");
        names.put("F", "Tom");
        names.put("G", "John");
        names.put("H", "John");
        names.put("I", "John");
        names.put("J", "Alice");

        return names;
    }

    public static void usunPowtorzoneImiona(Map<String, String> mapa) {
        HashMap<String, String> copy = new HashMap<String, String>(mapa);

        for (Map.Entry<String, String> pairA : copy.entrySet()) {
            for (Map.Entry<String, String> pairB : copy.entrySet()) {
                String value = pairB.getValue();
                if (pairA.getValue().equals(value) && !pairA.getKey().equals(pairB.getKey())) {
                    usunElementZMapPoWartosci(mapa, pairB.getValue());
                }
            }
        }
    }

    public static void usunElementZMapPoWartosci(Map<String, String> mapa, String wartosc) {
        HashMap<String, String> kopia = new HashMap<String, String>(mapa);
        for (Map.Entry<String, String> para : kopia.entrySet()) {
            if (para.getValue().equals(wartosc))
                mapa.remove(para.getKey());
        }
    }

    public static void main(String[] args) {

    }
}
