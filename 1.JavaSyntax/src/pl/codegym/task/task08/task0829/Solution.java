package pl.codegym.task.task08.task0829;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* 
Aktualizacja oprogramowania
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, String> adresy = new HashMap<>();

        while (true) {
            String miasto = reader.readLine();
            if (miasto.isEmpty()) break;

            String rodzina = reader.readLine();
            if (rodzina.isEmpty()) break;

            adresy.put(miasto, rodzina);
        }

        String miasto = reader.readLine();

        for (Map.Entry<String, String> pair : adresy.entrySet()) {
            if (pair.getKey().equals(miasto)) {
                System.out.println(pair.getValue());
            }
        }
    }
}
