package pl.codegym.task.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
Miła Emma i letnie wakacje
*/

public class Solution {
    public static HashMap<String, Date> utworzMap() throws ParseException {
        DateFormat df = new SimpleDateFormat("MMMMM d yyyy", Locale.forLanguageTag("pl"));
        HashMap<String, Date> mapa = new HashMap<String, Date>();

        mapa.put("ala", df.parse("MAJ 4 1989"));
        mapa.put("ola", df.parse("STYCZEŃ 5 1994"));
        mapa.put("ila", df.parse("GRUDZIEŃ 4 1991"));
        mapa.put("ali", df.parse("MAJ 4 1993"));
        mapa.put("pla", df.parse("MAJ 25 1994"));
        mapa.put("ALT", df.parse("LIPIEC 19 1984"));
        mapa.put("ALU", df.parse("SIERPIEŃ 12 1988"));
        mapa.put("ALE", df.parse("LIPIEC 4 1999"));
        mapa.put("TLA", df.parse("CZERWIEC 30 1998"));
        mapa.put("TLI", df.parse("LIPIEC 30 1998"));

        return mapa;
    }

    public static void usunUrodzonychLatem(HashMap<String, Date> mapa) {
        mapa.entrySet().removeIf(entry -> entry.getValue().getMonth() >= 5 && entry.getValue().getMonth() <= 7);
    }

    public static void main(String[] args) {
    }
}
