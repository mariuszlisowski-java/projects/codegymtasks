package pl.codegym.task.task08.task0815;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

/* 
Spis ludności
*/

public class Solution {
    public static HashMap<String, String> utworzMap() {
        HashMap<String, String> names = new HashMap<>();
        Random random = new Random();
        StringBuilder strA = new StringBuilder();
        StringBuilder strB = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                char charA = (char) (random.nextInt(25) + 97); // a-z [97, 122]
                char charB = (char) (random.nextInt(25) + 97); // a-z [97, 122]
                strA.append(Character.toString(charA));
                strB.append(Character.toString(charB));
            }
            names.put(strA.toString(), strB.toString());
            strA = new StringBuilder();
            strB = new StringBuilder();
        }

        return names;
    }

    public static int getLicznikTakichSamychImion(HashMap<String, String> map, String imie) {
        int counter = 0;
        for (String name : map.values()) {
            if (imie.equals(name)) {
                counter++;
            }
        }

        return counter;
    }

    public static int getLicznikTakichSamychNazwisk(HashMap<String, String> map, String nazwisko) {
        int counter = 0;
            for (String surname : map.keySet()) {
            if (nazwisko.equals(surname)) {
                counter++;
            }
        }

        return counter;
    }

    public static void main(String[] args) {

    }
}
