package pl.codegym.task.task08.task0819;

import java.util.HashSet;
import java.util.Set;

/* 
Koci Set
*/

public class Solution {
    public static void main(String[] args) {
        Set<Kot> koty = utworzKoty();

        Kot kot = null;
        if (!koty.isEmpty()) {
            kot = koty.iterator().next();
        }
        if (kot != null) {
            koty.remove(kot);
        }
        printKoty(koty);
    }

    public static Set<Kot> utworzKoty() {
        Set<Kot> koty = new HashSet<>();
        koty.add(new Kot());
        koty.add(new Kot());
        koty.add(new Kot());
        return koty;
    }

    public static void printKoty(Set<Kot> koty) {
        for (Kot kot : koty ) {
            System.out.println(kot);
        }
    }

    public static class Kot {}
}
