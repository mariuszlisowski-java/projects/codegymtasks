package pl.codegym.task.task08.task0821;

import java.util.HashMap;
import java.util.Map;

/* 
Współdzielone nazwiska i imiona
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, String> mapa = utworzMapLudzi();
        printMapLudzi(mapa);
    }

    public static Map<String, String> utworzMapLudzi() {
        Map<String, String> people = new HashMap<>();
        people.put("Surname0", "Name8");
        people.put("Surname0", "Name7");
        people.put("Surname1", "Name6");
        people.put("Surname2", "Name5");
        people.put("Surname3", "Name4");
        people.put("Surname4", "Name3");
        people.put("Surname5", "Name2");
        people.put("Surname6", "Name1");
        people.put("Surname7", "Name0");
        people.put("Surname8", "Name0");


        return people;
    }

    public static void printMapLudzi(Map<String, String> mapa) {
        for (Map.Entry<String, String> s : mapa.entrySet()) {
            System.out.println(s.getKey() + " " + s.getValue());
        }
    }
}
