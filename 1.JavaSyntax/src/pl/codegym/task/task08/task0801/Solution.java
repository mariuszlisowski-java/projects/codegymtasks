package pl.codegym.task.task08.task0801;

/* 
HashSet roślin
*/

import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static void main(String[] args) throws Exception {
        HashSet<String> mySet = new HashSet<>();
        mySet.add("arbuz");
        mySet.add("banan");
        mySet.add("wiśnia");
        mySet.add("gruszka");
        mySet.add("kantalupa");
        mySet.add("jeżyna");
        mySet.add("żeńszeń");
        mySet.add("truskawka");
        mySet.add("irys");
        mySet.add("ziemniak");

        for (String el : mySet) {
            System.out.println(el);
        }
    }
}
