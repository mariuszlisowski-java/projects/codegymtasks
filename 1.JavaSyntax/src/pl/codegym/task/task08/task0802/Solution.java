package pl.codegym.task.task08.task0802;

/* 
HashMap z 10 par
*/

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static void main(String[] args) throws Exception {
        HashMap<String, String> myMap = new HashMap<>();
        myMap.put("arbuz", "melon");
        myMap.put("banan", "owoc");
        myMap.put("wiśnia", "owoc");
        myMap.put("gruszka", "owoc");
        myMap.put("kantalupa", "melon");
        myMap.put("jeżyna", "owoc");
        myMap.put("żeńszeń", "korzeń");
        myMap.put("truskawka", "owoc");
        myMap.put("irys", "kwiat");
        myMap.put("ziemniak", "bulwa");

        for (HashMap.Entry<String, String> pair : myMap.entrySet() ) {
            System.out.println(pair.getKey() + " - " + pair.getValue());
        }
    }
}
