package pl.codegym.task.task08.task0828;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Numer miesiąca
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String monthName = bufferedReader.readLine();

        Map<String, Integer> months = new HashMap<String, Integer>() {{
            put("Styczeń", 1);
            put("Luty", 2);
            put("Marzec", 3);
            put("Kwiecień", 4);
            put("Maj", 5);
            put("Czerwiec", 6);
            put("Lipiec", 7);
            put("Sierpień", 8);
            put("Wrzesień", 9);
            put("Październik", 10);
            put("Listopad", 11);
            put("Grudzień", 12);
        }};

        for (Map.Entry<String, Integer> pair : months.entrySet()) {
            if (pair.getKey().equals(monthName)) {
                System.out.println(monthName + " to miesiąc nr " + pair.getValue());
            }
        }

    }
}
