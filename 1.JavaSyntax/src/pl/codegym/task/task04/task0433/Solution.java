package pl.codegym.task.task04.task0433;


/* 
Zobaczyć dolary w przyszłości
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        int i = 10, j;
        while (i > 0) {
            j = 10;
            while (j > 0) {
                System.out.print("$");
                --j;
            }
            System.out.println();
            --i;
        }
    }
}
