package pl.codegym.task.task04.task0424;

/* 
Trzy liczby
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int display;
        if (a == b) {
            display = 3;
        } else if (a == c) {
            display = 2;
        } else if (b == c) {
            display = 1;
        } else {
            return;
        }
        System.out.println(display);
    }
}
