package pl.codegym.task.task04.task0442;


/* 
Dodawanie
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int sum = 0;
        while (true) {
            String input = bufferedReader.readLine();
            sum += Integer.parseInt(input);
            if (input.equals("-1")) {
                break;
            }
        }
        System.out.println(sum);
    }
}
