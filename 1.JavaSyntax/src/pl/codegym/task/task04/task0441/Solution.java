package pl.codegym.task.task04.task0441;


/* 
Jakiś taki przeciętny
*/
import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int mid;
        if ((a >= b && a <= c) ||
            (a <= b && a >= c)) {
            mid = a;
        } else if ((b >= a && b <= c) ||
                   (b <= a && b >= c)) {
            mid = b;
        } else {
            mid = c;
        }
        System.out.println(mid);
    }
}
