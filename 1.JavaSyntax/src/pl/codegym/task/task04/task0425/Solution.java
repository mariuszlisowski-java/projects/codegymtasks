package pl.codegym.task.task04.task0425;

/* 
Target niedostępny!
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int square;
        if (a > 0 && b > 0) {
            square = 1;
        } else if (a < 0 && b > 0) {
            square = 2;
        } else if (a < 0 && b < 0) {
            square = 3;
        } else if (a > 0 && b < 0) {
            square = 4;
        } else {
            return;
        }
        System.out.println(square);
    }
}
