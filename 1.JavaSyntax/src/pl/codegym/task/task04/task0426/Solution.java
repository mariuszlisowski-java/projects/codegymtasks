package pl.codegym.task.task04.task0426;

/* 
Etykiety i liczby
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        if (number == 0) {
            System.out.println("Zero");
        } else {
            if (number % 2 == 0) {
                if (number < 0) {
                    System.out.println("Ujemna liczba parzysta");
                } else if (number > 0) {
                    System.out.println("Dodatnia liczba parzysta");
                }
            } else {
                if (number < 0) {
                    System.out.println("Ujemna liczba nieparzysta");
                } else if (number > 0) {
                    System.out.println("Dodatnia liczba nieparzysta");
                }
            }
        }
    }

}
