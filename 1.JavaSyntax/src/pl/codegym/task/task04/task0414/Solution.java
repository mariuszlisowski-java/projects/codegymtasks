package pl.codegym.task.task04.task0414;

/* 
Liczba dni w roku
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        int days;
        if (year % 400 == 0) {
            days = 366;
        } else if (year % 100 == 0){
            days = 365;
        } else if (year % 4 == 0) {
            days = 366;
        } else {
            days = 365;
        }

        System.out.println("Liczba dni w roku: " + days);
    }
}