package pl.codegym.task.task04.task0412;

/* 
Liczby dodatnie i ujemne
*/

import java.io.*;
import java.util.Enumeration;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int number;
        while (true) {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                break;
            }
            scanner.next();
        }
        if (number > 0) {
            number *= 2;
            System.out.println(number);
        } else if (number < 0) {
            number += 1;
            System.out.println(number);
        } else {
            System.out.println(number);
        }


    }

}