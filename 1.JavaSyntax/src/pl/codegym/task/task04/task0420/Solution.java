package pl.codegym.task.task04.task0420;

/* 
Sortowanie trzech liczb
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        if (a >= b && b >= c) {
            System.out.println(a + " " + b + " " + c);
        }
        if (a >= c && c > b) {
            System.out.println(a + " " + c + " " + b);
        }
        if (b >= c && c >= a) {
            System.out.println(b + " " + c + " " + a);
        }
        if (b > a && a > c) {
            System.out.println(b + " " + a + " " + c);
        }
        if (c > b && b >= a) {
            System.out.println(c + " " + b + " " + a);
        }
        if (c > a && a > b) {
            System.out.println(c + " " + a + " " + b);
        }
    }
}
