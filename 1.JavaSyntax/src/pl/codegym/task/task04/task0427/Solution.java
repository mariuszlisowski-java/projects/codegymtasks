package pl.codegym.task.task04.task0427;

/* 
Opisywanie liczb
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        if (number <= 0 || number > 999) {
            return;
        }

        int num = number,
            counter = 1;
        while (num >= 10) {
            num /= 10;
            ++counter;
        }

        if (number % 2 == 0) {
            if (counter == 1) {
                System.out.println("parzysta liczba jednocyfrowa");
            } else if (counter == 2) {
                System.out.println("parzysta liczba dwucyfrowa");
            } else if (counter == 3) {
                System.out.println("parzysta liczba trzycyfrowa");
            }
        } else {
            if (counter == 1) {
                System.out.println("nieparzysta liczba jednocyfrowa");
            } else if (counter == 2) {
                System.out.println("nieparzysta liczba dwucyfrowa");
            } else if (counter == 3) {
                System.out.println("nieparzysta liczba trzycyfrowa");
            }
        }

    }
}
