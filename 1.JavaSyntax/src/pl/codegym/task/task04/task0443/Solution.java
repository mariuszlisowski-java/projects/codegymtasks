package pl.codegym.task.task04.task0443;


/* 
Imię to imię
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String name = bufferedReader.readLine();
        int year = Integer.parseInt(bufferedReader.readLine());
        int month = Integer.parseInt(bufferedReader.readLine());
        int day = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Mam na imię " + name + ".");
        System.out.println("Urodziłem/am się " + month + "/" + day + "/" + year);
    }
}
