package pl.codegym.task.task04.task0429;

/* 
Liczby dodatnie i ujemne
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int neutral = 0;

        if (a == 0) {
            ++neutral;
        }
        if (b == 0) {
            ++neutral;
        }
        if (c == 0) {
            ++neutral;
        }

        if (neutral == 3) {
            System.out.println("Liczba liczb ujemnych: 0");
            System.out.println("Liczba liczb dodatnich: 0");
            return;
        }

        int negative = 0,
            positive = 0;

        if (a > 0) {
            ++positive;
        }
        if (b > 0) {
            ++positive;
        }
        if (c > 0) {
            ++positive;
        }
        negative = 3 - positive - neutral;

        System.out.println("Liczba liczb ujemnych: " + negative);
        System.out.println("Liczba liczb dodatnich: " + positive);
    }
}
