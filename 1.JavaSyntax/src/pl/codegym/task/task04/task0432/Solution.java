package pl.codegym.task.task04.task0432;



/* 
Dobrego nigdy zbyt wiele
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String text = bufferedReader.readLine();
        int number = Integer.parseInt(bufferedReader.readLine());

        while (number > 0) {
            System.out.println(text);
            --number;
        }

    }
}
