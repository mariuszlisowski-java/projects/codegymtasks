package pl.codegym.task.task04.task0416;

/* 
Przejście przez ulicę na oślep
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        double number = scanner.nextDouble();
        while (number >= 5) {
            number -= 5;
        }
        if (number >= 0 && number < 3) {
            System.out.println("zielone");
        }
        if (number >= 3 && number < 4) {
            System.out.println("żółte");
        }
        if (number >= 4 && number < 5) {
            System.out.println("czerwone");
        }
    }
}