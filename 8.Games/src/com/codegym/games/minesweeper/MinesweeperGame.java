package com.codegym.games.minesweeper;

import com.codegym.engine.cell.Color;
import com.codegym.engine.cell.Game;

import java.util.ArrayList;
import java.util.List;

public class MinesweeperGame extends Game {
    private static final int SIDE = 9;
    private GameObject[][] gameField = new GameObject[SIDE][SIDE];
    private int countMinesOnField;
    private static final String MINE = "\uD83D\uDCA3";
    private static final String FLAG = "\uD83D\uDEA9";
    private int countFlags;
    private boolean isGameStopped;
    private int countClosedTiles = SIDE * SIDE;
    private int score;

    @Override
    public void initialize() {
        setScreenSize(SIDE, SIDE);
        createGame();
    }

    private void createGame() {
        for (int y = 0; y < SIDE; y++) {
            for (int x = 0; x < SIDE; x++) {
                boolean isMine = getRandomNumber(10) < 1;
                if (isMine) {
                    countMinesOnField++;
                }
                gameField[y][x] = new GameObject(x, y, isMine);
                setCellColor(x, y, Color.ORANGE);
                setCellValue(x, y , "");
            }
        }
        countMineNeighbors();
        countFlags = countMinesOnField;
//        isGameStopped = false;
    }

    private void win() {
        isGameStopped = true;
        showMessageDialog(Color.BLUE, "Congratulations for the winner!", Color.WHITE, 30);
    }

    private void gameOver() {
        isGameStopped = true;
        showMessageDialog(Color.BLUE, "Game over!", Color.WHITE, 30);
    }

    private void markTile(int x , int y) {
        GameObject gameObject = gameField[y][x];
        if (!gameObject.isOpen && !isGameStopped) {
            if (!(countFlags == 0 && !gameObject.isFlag)) {
                if (!gameObject.isFlag) {
                    gameObject.isFlag = true;
                    setCellColor(x, y, Color.GREY);
                    setCellValue(x, y, FLAG);
                    --countFlags;
                } else {
                    gameObject.isFlag = false;
                    setCellColor(x, y, Color.ORANGE);
                    setCellValue(x, y, "");
                    ++countFlags;
                }
            }
        }
    }

    private void openTile(int x, int y) {
        GameObject gameObject = gameField[y][x];
        if (gameObject.isOpen || isGameStopped || gameObject.isFlag) {
            return;
        }
        if (gameObject.isMine) {
            setCellValueEx(x, y, Color.RED, MINE);
            gameOver();
        }
        gameObject.isOpen = true;
        --countClosedTiles;
        if (countClosedTiles == countMinesOnField && !gameObject.isMine) {
            win();
        }
        if (!gameObject.isMine) {
            score += 5;
            setScore(score);
        }
        setCellColor(x, y, Color.GREEN);
        if (gameObject.isMine) {
            setCellValue(x, y, MINE);
        } else if (gameObject.countMineNeighbors == 0) {
            setCellValue(x, y, "");
            for (GameObject neighbor : getNeighbors(gameObject)) {
                openTile(neighbor.x, neighbor.y);
            }
        } else {
            setCellNumber(x, y, gameObject.countMineNeighbors);
        }
    }

    private void restart() {
        isGameStopped = false;
        countClosedTiles = SIDE * SIDE;
        score = 0;
        setScore(score);
        countMinesOnField = 0;
        createGame();
    }

    @Override
    public void onMouseLeftClick(int x, int y) {
        if (isGameStopped) {
            restart();
        } else {
            openTile(x, y);
        }
    }

    @Override
    public void onMouseRightClick(int x, int y) {
        markTile(x, y);
    }

    private void countMineNeighbors() {
        for (int x = 0; x < SIDE; x++) {
            for (int y = 0; y < SIDE; y++) {
                GameObject gameObject = gameField[x][y];
                if (!gameObject.isMine) {
                    for (GameObject neighbor : getNeighbors(gameObject)) {
                        if (neighbor.isMine ) {
                            gameObject.countMineNeighbors++;
                        }
                    }
                }
            }
        }
    }

    private List<GameObject> getNeighbors(GameObject gameObject) {
        List<GameObject> result = new ArrayList<>();
        for (int y = gameObject.y - 1; y <= gameObject.y + 1; y++) {
            for (int x = gameObject.x - 1; x <= gameObject.x + 1; x++) {
                if (y < 0 || y >= SIDE) {
                    continue;
                }
                if (x < 0 || x >= SIDE) {
                    continue;
                }
                if (gameField[y][x] == gameObject) {
                    continue;
                }
                result.add(gameField[y][x]);
            }
        }
        return result;
    }
}